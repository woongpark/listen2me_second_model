package kr.co.l2me.cmmn;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RandomUtil {
	
	//logger
	private static final Logger log = LoggerFactory.getLogger(RandomUtil.class);
	
	public static String generateNumber(int length) {
	    String numStr = "1";
	    String plusNumStr = "1";
	 
	    for (int i = 0; i < length; i++) {
	        numStr += "0";
	 
	        if (i != length - 1) {
	            plusNumStr += "0";
	        }
	    }
	 
	    Random random = new Random();
	    int result = random.nextInt(Integer.parseInt(numStr)) + Integer.parseInt(plusNumStr);
	 
	    if (result > Integer.parseInt(numStr)) {
	        result = result - Integer.parseInt(plusNumStr);
	    }
	 
	    return String.valueOf(result);
	}
	
	public static void main(String[] args){
		log.debug("#first random value ==>{}", RandomUtil.generateNumber(3));
		log.debug("#second random value ==>{}", RandomUtil.generateNumber(3));
	}
}
