package kr.co.l2me.cmmn;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AESUtil {

	//logger
	private static final Logger log = LoggerFactory.getLogger(AESUtil.class);	
		 
//	final static String secretKey   = "12345678901234567890123456789012"; //32bit
	final static String secretKey   = "1234567890123456"; //16bit
	static String IV                = ""; //16bit
	 

	 public AESUtil(){
	     IV = secretKey.substring(0,16);
	 }
	 
	 //암호화
	 public String encode(String str) throws Exception{
	     byte[] keyData = secretKey.getBytes();
	 
	     SecretKey secureKey = new SecretKeySpec(keyData, "AES");
	 
	     Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
	     c.init(Cipher.ENCRYPT_MODE, secureKey, new IvParameterSpec(IV.getBytes()));
	 
	     byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
	     String enStr = new String(Base64.encodeBase64(encrypted));
	 
	     return enStr;
	 }
	 
	 //복호화
	 public String decode(String str) throws Exception{
		 byte[] keyData = secretKey.getBytes();
		 SecretKey secureKey = new SecretKeySpec(keyData, "AES");
		 Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		 c.init(Cipher.DECRYPT_MODE, secureKey, new IvParameterSpec(IV.getBytes("UTF-8")));
	 
		 byte[] byteStr = Base64.decodeBase64(str.getBytes());
	 
		 return new String(c.doFinal(byteStr),"UTF-8");
	 }
		
	 public static void main(String[] args){
		 String planText 	= "admin";
		 String cipherText	= null;	
		 try{
			 AESUtil aes = new AESUtil();
			 
			 cipherText 	= aes.encode(planText);
			 log.debug("#==>{}" ,  cipherText);
			 log.debug("#==>{}" ,  aes.decode(cipherText));
		 }catch(Exception e){
			 log.debug("",e);
		 }
	 }
}
