package kr.co.l2me.cmmn;

import egovframework.rte.fdl.cmmn.exception.handler.ExceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <pre>
 * kr.co.cmmn 
 *    |_ CommonOthersExcepHndlr.java
 * 
 * </pre>
 * @date : 2017. 8. 16. 오후 7:04:09
 * @version : 
 * @author : wpark
 */
public class CommonOthersExcepHndlr implements ExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommonOthersExcepHndlr.class);

	/**
	 * 
	 */
	@Override
	public void occur(Exception exception, String packageName) {
		LOGGER.debug(" EgovServiceExceptionHandler run...............");
	}

}
