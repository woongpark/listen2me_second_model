package kr.co.l2me.cmmn;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MailUtil {
	
	private static final Logger log = LoggerFactory.getLogger(MailUtil.class);
	
	private String host ;
	private String user ;
	private String password	;
	
	public MailUtil(String host , String user , String password){
		this.host = host;
		this.user = user;
		this.password = password;
	}
	
	public void send( String receive , String title , String content){
		// Get the session object
	  Properties props = new Properties();
	  props.put("mail.smtp.host", host);
	  props.put("mail.smtp.auth", "true");
	  
	  Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
		   protected PasswordAuthentication getPasswordAuthentication() {
		    return new PasswordAuthentication(user, password);
		   }
	  });
	  
	// Compose the message
	  try {
	   MimeMessage message = new MimeMessage(session);
	   message.setFrom(new InternetAddress(user));
	   message.addRecipient(Message.RecipientType.TO, new InternetAddress(receive));

	   // Subject
	   message.setSubject(title);
	   
	   // Text
	   message.setText(content);

	   // send the message
	   Transport.send(message);
	   log.debug("message sent successfully...");

	  } catch (MessagingException e) {
		  log.debug("",e);
	  }
	  
	}
}
