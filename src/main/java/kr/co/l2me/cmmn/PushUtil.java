package kr.co.l2me.cmmn;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PushUtil {

	//logger
	private static final Logger log = LoggerFactory.getLogger(PushUtil.class);
	
	private String authKey ;
	
	public PushUtil(String authKey) {
		this.authKey = authKey;
	}
	
	public void SendPush(String url , String message ) throws Exception {
		HttpClient 	client	 	= HttpClientBuilder.create().build();
		HttpPost 	post		= new HttpPost(url);
		
		post.setHeader("Authorization"	, "key="+ this.authKey);
		post.setHeader("Content-Type"	, "application/json; charset=UTF-8"); 
		
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		String 					responseBoday	= null;
		
		StringEntity 			StringEntity	= new StringEntity( message , ContentType.APPLICATION_JSON );
		
		post.setEntity(StringEntity);
		
		//HttpResponse response = client.execute(post , responseHandler);
		//log.debug("#==>{}" , response.getStatusLine().getStatusCode());
		responseBoday = client.execute(post , responseHandler);
		log.debug("==>{}" , responseBoday);
	}
	
	public static void main(String[] args){
		String url 	= "https://fcm.googleapis.com/fcm/send";
		String keys	= "AAAA2-Xle1U:APA91bFlTamN_v0OspksCL5tLScDvKNoSlDMaKO8lGklZ5HuelOhT2COHNVOdrEXeFhJqDs9wz3qodoIbDChi4c3uRfizYv5N34LZ9Q3G61LkVA5_jrZ0vzQS5pw0hALWSuYQlU7dkjs";
		
//		String message = "{\"registration_ids\":[\"cxaPo2mdUlo:APA91bHs6WhWrokKEs4QSQ8hx-je_6ao9C_ig_9ttmqnPkshz8QwHSs6UX7vASc6fzZCHpxdIxopxT6o5RkxdBMN1tZ_q6EgdUZAoFK8dznO-v_ddKQhzKnTCcGHYxjwaASnQ4WiCnhp\"]}";
		
//		String message = "{\"to\" : \"cxaPo2mdUlo:APA91bHs6WhWrokKEs4QSQ8hx-je_6ao9C_ig_9ttmqnPkshz8QwHSs6UX7vASc6fzZCHpxdIxopxT6o5RkxdBMN1tZ_q6EgdUZAoFK8dznO-v_ddKQhzKnTCcGHYxjwaASnQ4WiCnhp\"";
		String message = "{\"to\" : \"eOrfRq2wvMo:APA91bEiQSlsbfKMWEKOdoIg-NxagJH2sRJ5AaaPMSh1ILc2J71BSIQA8fsi7OR5jYzObyg6WVlhPvcCfMxtDg1YNnZ2Gp6ydDHX8dEK9cLD9JxCvgpEIZB0wzw-LF7IIF_zzix8mPUf\"";
		message += ",\"notification\":{\"message\":\"한글테스트입니다...\"}";
		message += "}";
		
		PushUtil push = new PushUtil(keys);
		try {
			push.SendPush(url, message);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
