package kr.co.l2me.cmmn;

import egovframework.rte.fdl.cmmn.exception.handler.ExceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <pre>
 * kr.co.cmmn 
 *    |_ CommonExcepHndlr.java
 * 
 * </pre>
 * @date : 2017. 8. 16. 오후 7:04:03
 * @version : 
 * @author : wpark
 */
public class CommonExcepHndlr implements ExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommonExcepHndlr.class);


	/**
	 * 
	 */
	@Override
	public void occur(Exception ex, String packageName) {
		LOGGER.debug(" CommonExcepHndlr run...............");
	}
}
