package kr.co.l2me.cmmn;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RSAUtil {

	//logger
	private static final Logger log = LoggerFactory.getLogger(RSAUtil.class);
	
	//key size
	private static final int 	DEFAULT_KEY_SIZE 		= 1024;
	//alogrithm
	private static final String KEY_FACTORY_ALGORITHM 	= "RSA";
	//char set
	private static final String CHARSET					= "UTF-8";
	
	private KeyPair pair = null;
	
	public RSAUtil(){
		init();
	}
	
	public void init() {
		try {
			generateKeyPair();
		} catch (Exception e) {
			log.debug("",e);
		}
	}
	
	//RSA key pair generate
    private KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance(KEY_FACTORY_ALGORITHM);
        generator.initialize(DEFAULT_KEY_SIZE, new SecureRandom());
        pair = generator.generateKeyPair();
        return pair;
    }
   
    //get string public key
   public String getPublicKey(){
	   
	   if (pair == null) {
		   return null;
	   }
	   byte[] key = pair.getPublic().getEncoded();
	   
	   return Base64.encodeBase64String(key);
   }
   
   //get string private key
   public String getPrivateKey(){
	   if (pair == null) {
		   return null;
	   }
	   byte[] key = pair.getPrivate().getEncoded();
	   
	   return Base64.encodeBase64String(key);
   }
   
   //encrypte
   public String encrypt(String planText , String key) throws Exception{
	   
	   if (key == null || key.equals("")) {
		   return null;
	   }
	   
	   byte[] encodedPublicKey = Base64.decodeBase64(key);
	   
	   KeyFactory 	keyFactory 	= KeyFactory.getInstance(KEY_FACTORY_ALGORITHM);
	   PublicKey	publicKye	= keyFactory.generatePublic(new X509EncodedKeySpec(encodedPublicKey));
	   
	   Cipher 		cipher		= Cipher.getInstance(KEY_FACTORY_ALGORITHM);
	   cipher.init(Cipher.ENCRYPT_MODE, publicKye);
	   
	   byte[] bytes = cipher.doFinal(planText.getBytes(CHARSET));
	   
	   return Base64.encodeBase64String(bytes);
   }
   
   //decrypt
   public String decrypt(String cipherText , String key) throws Exception {
	   if (key == null || key.equals("")) {
		   return null;
	   }
	   
	   byte[] encodedPrivateKey = Base64.decodeBase64(key);
	   
	   KeyFactory 	keyFactory 	= KeyFactory.getInstance(KEY_FACTORY_ALGORITHM);
	   PrivateKey	privateKey	= keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encodedPrivateKey));
	   
	   byte[]		bytes		= Base64.decodeBase64(cipherText);
	   
	   Cipher 		cipher		= Cipher.getInstance(KEY_FACTORY_ALGORITHM);
	   cipher.init(Cipher.DECRYPT_MODE, privateKey);
	   
	   return new String(cipher.doFinal(bytes) , CHARSET);
   }
   
   public byte[] decryptByte(String cipherText , String key) throws Exception {
	   if (key == null || key.equals("")) {
		   return null;
	   }
	   
	   byte[] encodedPrivateKey = Base64.decodeBase64(key);
	   
	   KeyFactory 	keyFactory 	= KeyFactory.getInstance(KEY_FACTORY_ALGORITHM);
	   PrivateKey	privateKey	= keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encodedPrivateKey));
	   
	   byte[]		bytes		= Base64.decodeBase64(cipherText);
	   
	   Cipher 		cipher		= Cipher.getInstance(KEY_FACTORY_ALGORITHM);
	   cipher.init(Cipher.DECRYPT_MODE, privateKey);
	   byte[] plan = cipher.doFinal(bytes) ;
	   
	   return plan;
   }   
   
   //List
   public String decryptBlock(String cipherText , String key) throws Exception {
	   
	   if (key == null || key.equals("")) {
		   return null;
	   }
	   
	   String[] blocks = cipherText.split("=");
	   
	   byte[] data = new byte[2048];
	   
	   int staPos 		= 0;
	   int blockSize 	= blocks.length;
	   
	   for ( int i = 0 ; i < blockSize ; i++) {
		   byte[] cipherTxt = null;
		   if ( i == (blockSize - 1)){
			   cipherTxt = decryptByte( blocks[i] , key);
			   System.arraycopy(cipherTxt, 0 , data , staPos , cipherTxt.length );
		   }
		   else {
			   cipherTxt = decryptByte( blocks[i] , key);
			   System.arraycopy(cipherTxt, 0 , data , staPos , cipherTxt.length);
		   }

		   staPos += 100;
	   } 
	   
	   return new String(data, CHARSET);
   }
   
   public static void main(String[] args){
	   try{
		   String planText 		= "{}";
		   String cipherText	= null;
		   
		   RSAUtil rsa 	= new RSAUtil();
		   
		   String publicKey 	= rsa.getPublicKey();
		   String privateKey	= rsa.getPrivateKey();
		   
		   cipherText			= rsa.encrypt(planText, publicKey);
		   log.debug("#encode ==> {}" , cipherText);
		   
		   log.debug("#decode ==> {}" , rsa.decrypt(cipherText, privateKey));
		   
	   }catch(Exception e){
		   log.debug("",e);
	   }
	   
   }
}
