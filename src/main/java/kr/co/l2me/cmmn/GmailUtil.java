package kr.co.l2me.cmmn;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GmailUtil {
	
	private static final Logger log = LoggerFactory.getLogger(GmailUtil.class);
	
	private String host ;
	private String user ;
	private String password	;
	
	public GmailUtil(String host , String user , String password){
		this.host = host;
		this.user = user;
		this.password = password;
	}
	
	public void send( String receive , String title , String content) throws Exception{
		// Get the session object
	  Properties props = new Properties();
	  
	  props.put("mail.smtp.host", host); 
      props.put("mail.smtp.port", "465");  
      props.put("mail.debug", "true"); 
      props.put("mail.smtp.auth", "true"); 
      props.put("mail.smtp.starttls.enable","true"); 
      props.put("mail.smtp.EnableSSL.enable","true");
      props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");   
      props.put("mail.smtp.socketFactory.port", "465"); 
      props.put("mail.smtp.socketFactory.fallback", "false");   
 	  
	  Session session = Session.getInstance(props, new javax.mail.Authenticator() {
		   protected PasswordAuthentication getPasswordAuthentication() {
		    return new PasswordAuthentication(user, password);
		   }
	  });
	  
	// Compose the message
	  try {
	   MimeMessage message = new MimeMessage(session);
	   message.setFrom(new InternetAddress(user));
	   message.addRecipient(Message.RecipientType.TO, new InternetAddress(receive));

	   // Subject
	   message.setSubject(title, "utf-8");
	   
	   // Text
	   message.setText(content, "utf-8");

	   // send the message
	   Transport.send(message);
	   log.debug("message sent successfully...");

	  } catch (MessagingException e) {
		  log.debug("",e);
	  }
	  
	}
	
	public static void main(String[] args) throws Exception{
		
		String host 	= "smtp.gmail.com";
		String user 	= "l2meuser@connectx.co.kr";
		String password	= "0816cntx";
		
		GmailUtil gmail = new GmailUtil(host,user,password);
		
		String receive 	= "krona@naver.com";
		String title	= "[리슨투미] 인증코드 안내입니다.";
		String content	= "인증번호 : 테슽.";
		
		gmail.send(receive, title, content);
	}
}
