package kr.co.l2me.cmmn;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base32;
import org.apache.http.impl.client.TunnelRefusedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OtpUtil {
	
	/** log class */
	private static final Logger log = LoggerFactory.getLogger(OtpUtil.class);
	
	private String host = "listen2me.or.kr";
	
	/** constructor	 */
	public OtpUtil(){}
	
	public Map<String,String> getEncodedKey(String id){
		
		HashMap<String,String> map = new HashMap<String,String>();
		
		String firstKey 	= getKey();
		String secondKey	= getKey();
		
		String encodedKey   = firstKey + secondKey;
		
		log.debug("#encodedKey ==>{}" , encodedKey);
		
		String url = getQRBarcodeURL( id , host , encodedKey);
		log.debug("#getQRBarcodeURL ==>{}" , url);
		
		map.put("encodedKey", encodedKey);
		map.put("url", url);
		
		return map;
	}
	
	public String getKey(){
		byte[] buffer = new byte[5*5*5];
		new Random().nextBytes(buffer);
		
		Base32 codec = new Base32();
		
		byte[] secretKey 	= Arrays.copyOf(buffer, 5);
		byte[] bEncodeKey	= codec.encode(secretKey);
		
		String encodedKey	= new String(bEncodeKey);
		
		return encodedKey;
	}
	
	public String getQRBarcodeURL( String id , String host , String secret){
//		String format = "http://chart.apis.google.com/chart?cht=qr&amp;chs=300x300&amp;chl=otpauth://totp/%s@%s%%3Fsecret%%3D%s&amp;chld=H|0";
		String format = "http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl=otpauth://totp/%s@%s%%3Fsecret%%3D%s&chld=H|0";
		return String.format(format, id , host , secret);
	}
	
	public boolean checkCode(String secret , String pin ) throws Exception{
				
		long l = new Date().getTime();
        long ll =  l / 30000;
		
        long usercode = Integer.parseInt(pin);
        
        Base32 codec = new Base32();
        byte[] decodedKey = codec.decode(secret);
        
        int window = 3;
        
        for (int i = -window ; i <= window ; ++i) {
        	long hash = verifyCode(decodedKey, ll + i);
        	
        	if (hash == usercode) {
        		return true;
        	}
        }
        
		return false;
	}
	
	public int verifyCode(byte[] key , long t) throws Exception{
		byte[] 	data 	= new byte[8];
		long	value 	= t;
		
		for (int i = 8 ; i-- > 0 ; value >>>= 8) {
			data[i] = (byte)value;
		}
		
		SecretKeySpec signKey = new SecretKeySpec(key, "HmacSHA1");
		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(signKey);
		
		byte[] hash = mac.doFinal(data);
		
		int offset = hash[20-1] & 0xF;
		
		long truncatedHash = 0;
		
		for (int i = 0 ; i < 4 ; ++i) {
			truncatedHash <<= 8 ;
			truncatedHash |= (hash[offset +i] & 0xFF);
		}
		
		truncatedHash &= 0x7FFFFFFF;
		truncatedHash %= 1000000;
		
		return (int)truncatedHash ;
	}
}
