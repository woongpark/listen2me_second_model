package kr.co.l2me.cmmn;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.web.servlet.view.document.AbstractExcelView;

public class ExcelDownload extends AbstractExcelView {

	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
				
		 // get data model which is passed by the Spring container
        List<Map<String,Object>> historys = (List<Map<String,Object>>) model.get("date");
         
        // create a new Excel sheet
        HSSFSheet sheet = workbook.createSheet("statics");

        sheet.setDefaultColumnWidth(30);
         
        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
         
        // create header row
        HSSFRow header = sheet.createRow(0);
         
        header.createCell(0).setCellValue("일자");
        header.getCell(0).setCellStyle(style);
         
        header.createCell(1).setCellValue("DATA");
        header.getCell(1).setCellStyle(style);
         
        // create data rows
        int rowCount = 1;
         
        for (Map<String,Object> record : historys) {
            HSSFRow aRow = sheet.createRow(rowCount++);
            aRow.createCell(0).setCellValue((String)record.get("date"));
            aRow.createCell(1).setCellValue(String.valueOf(record.get("cnt")));
        }	
	}
}
