package kr.co.l2me.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovDateUtil;
import kr.co.l2me.admin.mapper.CommonMapper;
import kr.co.l2me.admin.mapper.ReportMapper;
import kr.co.l2me.admin.vo.CustomUserDetails;
import kr.co.l2me.admin.vo.Paging;
import kr.co.l2me.cmmn.AESUtil;
import kr.co.l2me.cmmn.PushUtil;
import kr.co.l2me.cmmn.RSAUtil;

@Controller
public class ReportController {
	
	//Logger
	private static final Logger log = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	private ReportMapper mapper;
	
	@Autowired
	private CommonMapper commonMapper;
	
	@Autowired
	EgovPropertyService propertyService;
	
	@RequestMapping(value="/admin/getreports.do")
	public String getReports(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) {
		
		if (log.isDebugEnabled()){
			log.debug("#getReports page.......");
			log.debug("#param==>{}" , param);
		}
		
		String cPage = (String)param.get("cPage");
		
		int staPos   = 1 ;
		
		if ( cPage != null && !cPage.equals("1") ) {
			staPos = (Integer.parseInt(cPage) - 1) * 10;
		}
		else{
			cPage = "1";
//			staPos = 0;
		}
		
		int totalCount = mapper.getReportCnt();
		
        Paging paging = new Paging();
        paging.setPageNo(Integer.parseInt(cPage));
        paging.setPageSize(10);
        paging.setTotalCount(totalCount);

        param.put("staPos", staPos);
		
        log.debug("#==>{}" , param);
        log.debug("#paging==>{}" , paging.toString());
        
		List<Map<String,Object>> reports = mapper.getReportDetails(param);
		
		model.addAttribute("reports"	, reports);
		model.addAttribute("paging"	, paging);
		
		return "/reports";
	}
	
	@RequestMapping(value="/admin/getreport.do")
	public String getReport(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) {
		
		if (log.isDebugEnabled()){
			log.debug("#getReport page.......");
			log.debug("#param==>{}" , param);
		}
						
		List<Map<String,Object>> reports = mapper.getReportDetails(param);
		
		Map<String,Object> report = reports.get(0);
		
		log.debug("#report==>{}",report);
		
		model.addAttribute("report"	, report);
		
		return "/report";
	}
	
	@RequestMapping(value="/admin/setreportstatus.do")
	@ResponseBody
	public Map<String,Object> setReportStatus(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) throws Exception{
		
		log.debug("#setReportStatus.do start......");
		log.debug("#param==>{}", param.toString());
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id = userDetails.getUsername();
		
		String toDay = EgovDateUtil.getCurrentDateAsString();
		
		Map<String,Object> parameter = new HashMap<String,Object>();
		
		String reportSeq 	= (String)param.get("reportSeq");
		String status 		= (String)param.get("status");
		String reason		= (String)param.get("reason");
		
		parameter.put("reportSeq"	, reportSeq);
		parameter.put("rgsId"		, id);
		
		int iRlt = 0;
		
		int iReportCnt = mapper.getReportStatusCnt(parameter);
		
		List reports = mapper.getReportDetails(parameter);
		Map<String,Object> reportInfo = (Map<String,Object>)reports.get(0);
		
		String 	title		= null;
		String 	token 		= (String)reportInfo.get("token");
		String	groupId 	= (String)reportInfo.get("surveyGroupId");
		String  surveyId	= (String)reportInfo.get("surveyId");
		String 	reportTlt	= (String)reportInfo.get("title");
		
		//신고확인
		if ("C".equals(status)) {
			parameter.put("reportStatus"	, status);
			parameter.put("confirmDt"		, toDay);
		}
		//사건접수/조사중
		else if ("I".equals(status)){
			parameter.put("reportStatus"	, status);
			parameter.put("serveyDt"		, toDay);
			parameter.put("serveyTxt"		, reason);
			
			title = "[처리 안내] " + reportTlt +" 건에 대한 조사가 진행 중입니다.";
			sendPush(token, title, Integer.parseInt(surveyId), Integer.parseInt(groupId), 7000);
		}
		//처분대기
		else if ("W".equals(status)){
			parameter.put("reportStatus"	, status);
			parameter.put("resultDt"		, toDay);
			parameter.put("resultTxt"		, reason);
		}
		//완결
		else if ("F".equals(status)){
			parameter.put("reportStatus"	, status);
			
			title = "[처리 안내] " + reportTlt +" 건에 대한 처리가 완료되었습니다.";
			sendPush(token, title, Integer.parseInt(surveyId), Integer.parseInt(groupId), 8000);
		}
		
		if (iReportCnt == 0 ) {
			iRlt = mapper.setReportDtail(parameter);
		}
		else {
			iRlt = mapper.updateReportDetail(parameter);
		}
		
		Map<String,Object> result = new HashMap<String,Object>();
				
		if (iRlt > 0 ) {
			result.put("code"	, "0000");
			result.put("msg"	, "SUCCESS....");
		}
		else {
			result.put("code"	, "9999");
			result.put("msg"	, "fail....");
		}
				
		return result;
	}
	
	@RequestMapping(value="/admin/getreportdetail.do")
	public String getreportDetail(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) throws Exception{
		
		log.debug("#getreportdetail.do start......");
		log.debug("#param==>{}", param.toString());
				
		List<Map<String,Object>> 	reports = mapper.getReports(param);
		Map<String,Object> 			report	= reports.get(0); 
		
		String						surveyData	= (String)report.get("surveyData");
		
		log.debug("##==>{}" , surveyData);
		
		Map<String,Object> 			keys	= commonMapper.getKeys(null);
		String						key		= (String)keys.get("privateKey");
		
		RSAUtil 	rsaUtil 	= new RSAUtil();
		String 		jsonData 	= rsaUtil.decryptBlock(surveyData, key);
		
		log.debug("==>{}" , jsonData );
		
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String,Object> planJson = objectMapper.readValue(jsonData, Map.class);
		
		model.addAttribute("planJson"	, planJson);
		model.addAttribute("param"		, param);
		
		//attacker
//		Map<String,Object> 			parameter = new HashMap<String,Object>();
//		parameter.put("attackerSeq", 26);
//		
//		Map<String,Object>			attatcker = commonMapper.getAttacker(parameter);
//		surveyData	= (String)attatcker.get("attackerInfo");
//		jsonData 	= rsaUtil.decryptBlock(surveyData, key);
//		log.debug("==>{}" , jsonData );
//		List<Map<String,Object>> attackers = objectMapper.readValue(jsonData, List.class);
//		log.debug("==>{}" , attackers);
		//attacker
		
		return "/reportdetail";
	}
	
	public void sendPush(String token , String title , int id , int groupId , int actionId){
		String url		= propertyService.getString("fcm_url");
		String authKey  = propertyService.getString("fcm_key");
		
		PushUtil pushUtil = new PushUtil(authKey);
		
		Map<String,Object> messages = new HashMap<String,Object>();
		messages.put("to", token);
		
		Map<String,Object> notification = new HashMap<String,Object>();
		notification.put("body"		, title);
		notification.put("title"	, "");
		notification.put("icon"		, "");
		
		Map<String,Object> data = new HashMap<String,Object>();
		data.put("id"			, id);
		data.put("group_id"		, groupId);
		data.put("action_id"	, actionId);
		
		messages.put("notification"	, notification);
		messages.put("data"			, data);

		try {
			ObjectMapper mapper = new ObjectMapper();
			String str = mapper.writeValueAsString(messages);
			pushUtil.SendPush(url, str);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
