package kr.co.l2me.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.l2me.admin.mapper.BoardMapper;
import kr.co.l2me.admin.vo.CustomUserDetails;
import kr.co.l2me.admin.vo.Paging;

@Controller
public class BoardController {
	
	//Logger
	private static final Logger log = LoggerFactory.getLogger(BoardController.class);
	
	@Autowired
	BoardMapper boardMapper;
	
	@RequestMapping(value="/admin/getboards.do")
	public String getBoards(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) {
		log.debug("#getboards page.......");
		
		int totalCount = boardMapper.getBoardCnt();
		
		String cPage = (String)param.get("cPage");
		
		int staPos   = 0 ;
		
		if (cPage != null ) {
			staPos = Integer.parseInt(cPage) * 10;
		}
		else{
			cPage = "1";
		}
		
        Paging paging = new Paging();
        paging.setPageNo(Integer.parseInt(cPage));
        paging.setPageSize(10);
        paging.setTotalCount(totalCount);

        param.put("staPos", staPos);
		
		List<Map<String,Object>> boards = boardMapper.getBoards(param);
		
		model.addAttribute("boards"	, boards);
		model.addAttribute("paging"	, paging);
		
		return "/boards";
	}
	
	@RequestMapping(value="/admin/addboard.do")
	public String addBoard(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model) {
		log.debug("#addboard page.......");
				
		return "/addboard";
	}
	
	@RequestMapping(value="/admin/updatepage.do")
	public String updatePage(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) {
		log.debug("#updatePage page.......");
		
		Map<String,Object> board = boardMapper.getBoard(param);
		model.addAttribute("board" , board);
		return "/updateboard";
	}
	
	@RequestMapping(value="/admin/setboard.do")
	@ResponseBody
	public Map<String,Object> setBoard(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model
			, @RequestParam Map<String,Object> param) {
		
		log.debug("#setboard.do.....");
		log.debug("#param==>{}" , param);
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		String id			= userDetails.getUsername();
		
		String title	 	= (String) param.get("title");
		String content		= (String) param.get("content");
		//즉시 유무
		String displayEn 	= (String) param.get("displayEn");
		String reserveDt	= (String) param.get("reserveDt");
		
		if ( "Y".equals(displayEn) ) {
			reserveDt = null;
		}
		else {
			displayEn = "N";
		}
		
		Map<String,Object> parameter = new HashMap<String,Object>();
		parameter.put("title"		, title);
		parameter.put("content"		, content);
		parameter.put("diplayEn"	, displayEn);
		parameter.put("reserveDt"	, reserveDt);
		parameter.put("rgsId"		, id);
		
		int iRlt = boardMapper.setBoard(parameter);
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		if (iRlt > 0 ){
			result.put("code" , "0000");
			result.put("msg"  , "SUCCESS");
		}
		else {
			result.put("code" , "9999");
			result.put("msg"  , "FAILED");
		}
		
		return result;
	}
	
	@RequestMapping(value="/admin/deleteboard.do")
	@ResponseBody
	public Map<String,Object> deleteBoard(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model
			, @RequestParam Map<String,Object> param) {
		
		log.debug("#delete.do.....");
		log.debug("#param==>{}" , param);
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		String id			= userDetails.getUsername();						
		String boardId	 	= (String) param.get("boardId");

		
		Map<String,Object> parameter = new HashMap<String,Object>();
		parameter.put("boardId"		, boardId);
		parameter.put("rgsId"		, id);
		
		int iRlt = boardMapper.deleteBoard(parameter);
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		if (iRlt > 0 ){
			result.put("code" , "0000");
			result.put("msg"  , "SUCCESS");
		}
		else {
			result.put("code" , "9999");
			result.put("msg"  , "FAILED");
		}
		
		return result;
	}
	
	@RequestMapping(value="/admin/updateboard.do")
	@ResponseBody
	public Map<String,Object> updateBoard(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model
			, @RequestParam Map<String,Object> param) {
		
		log.debug("#updateBoard.do.....");
		log.debug("#param==>{}" , param);
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		String id			= userDetails.getUsername();
			
		String boardId		= (String) param.get("boardId");
		String title	 	= (String) param.get("title");
		String content		= (String) param.get("content");
		
		//즉시 유무
		String displayEn 	= (String) param.get("displayEn");
		String reserveDt	= (String) param.get("reserveDt");
		
		if ( "Y".equals(displayEn) ) {
			reserveDt = null;
		}
		else {
			displayEn = "N";
		}
				
		Map<String,Object> parameter = new HashMap<String,Object>();
		parameter.put("boardId"		, boardId);
		parameter.put("title"		, title);
		parameter.put("content"		, content);
		parameter.put("diplayEn"	, displayEn);
		parameter.put("reserveDt"	, reserveDt);
		parameter.put("rgsId"		, id);
		
		int iRlt = boardMapper.updateBoard(parameter);
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		if (iRlt > 0 ){
			result.put("code" , "0000");
			result.put("msg"  , "SUCCESS");
		}
		else {
			result.put("code" , "9999");
			result.put("msg"  , "FAILED");
		}
		
		return result;
	}
}
