package kr.co.l2me.admin.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.l2me.admin.mapper.InstallHitMapper;
import kr.co.l2me.admin.mapper.SettingMapper;
import kr.co.l2me.admin.vo.CustomUserDetails;
import kr.co.l2me.cmmn.ExcelDownload;

@Controller
public class StaticsController {
	
	//logger
	private static final Logger log = LoggerFactory.getLogger(StaticsController.class);
	
	//setting mapper
	@Autowired
	SettingMapper settingMapper ;
	
	@Autowired
	InstallHitMapper installHitMapper;
		
	@RequestMapping(value="/admin/getstatics.do")
	public String getStatics(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		
		if (log.isDebugEnabled()){
			log.debug("#getStatics.do.....");
		}
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id = userDetails.getUsername();
		
		param.put("chgId", id);
		
		Map<String,Object> setting = settingMapper.getSetting(param);
		
		String prdCd = (String)setting.get("prdCd");
		
		String item = (String)setting.get("item");
		
		String[] items = item.split("[|]");
		
		log.debug("#prdCd ==>{}" , prdCd);
		log.debug("#item  ==>{}" , item);
		// D , W , M , Y

		List<Map<String,Object>> all 	= installHitMapper.getDayWeeks(param);
		
		//basic
		String siteId = userDetails.getSiteId();
		siteId = "app0000020";
		
		param.put("siteId", siteId);
		List<Map<String,Object>> basic 	= installHitMapper.getDayWeeks(param);
		
		model.addAttribute("all", all);
		model.addAttribute("basic", basic);		
		
		return "/stc/statics";
	}
	
	@RequestMapping(value="/admin/getallstc.do")
	public String getAllStatics(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		
		if (log.isDebugEnabled()){
			log.debug("#getAllStatics.do.....");
		}
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id = userDetails.getUsername();
		
		param.put("chgId", id);
		
		//setting
		Map<String,Object> setting = settingMapper.getSetting(param);
		
//		String prdCd = (String)setting.get("prdCd");
		String item = (String)setting.get("item");
		
		String[] items = item.split("[|]");
		
		String prdCd = (String)param.get("priod");
		
		if (prdCd == null || prdCd.equals("")) {
			prdCd = (String)setting.get("prdCd");
		}
		
		log.debug("#prdCd ==>{}" , prdCd);
		log.debug("#item  ==>{}" , item);
		// D , W , M , Y

		
		//기간
		String startDd  = (String)param.get("startDd");
		String endDd	= (String)param.get("endDd");
		
		if (startDd == null || startDd.equals("")) {
			Map<String,Object> period = installHitMapper.getWeekPeriod();
			
			startDd = (String)period.get("seven");
			endDd	= (String)period.get("today");
		}
		
		param.put("startDd"	, startDd);
		param.put("endDd"	, endDd);
		
		
		List<Map<String,Object>> all 	= null;
		
		if (prdCd.equals("D")) {
			all 	= installHitMapper.getDays(param);
		}
		else if (prdCd.equals("W")) {
			all 	= installHitMapper.getWeeks(param);
		}
		else if (prdCd.equals("M")) {
			all	 	= installHitMapper.getMonths(param);
		}
		else if (prdCd.equals("Y")) {
			all		= installHitMapper.getYears(param);
		}
				
		
		model.addAttribute("priod",prdCd);
		model.addAttribute("all", all);
		
		model.addAttribute("startDd", startDd);
		model.addAttribute("endDd"	, endDd);	
		
		return "/stc/allstc";
	}
	
	@RequestMapping(value="/admin/alldownloadExcel.do")
	public ModelAndView getAllDownload(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		
		if (log.isDebugEnabled()){
			log.debug("#getAllDownload.do.....");
		}
		
		response.setHeader("Content-disposition", "attachment; filename=all.xls"); //target명을 파일명으로 작성
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id = userDetails.getUsername();
		
		param.put("chgId", id);
		
		//setting
		Map<String,Object> setting = settingMapper.getSetting(param);
		
//		String prdCd = (String)setting.get("prdCd");
		String item = (String)setting.get("item");
		
		String[] items = item.split("[|]");
		
		String prdCd = (String)param.get("priod");
		
		if (prdCd == null || prdCd.equals("")) {
			prdCd = (String)setting.get("prdCd");
		}
		
		log.debug("#prdCd ==>{}" , prdCd);
		log.debug("#item  ==>{}" , item);
		// D , W , M , Y

		
		//기간
		String startDd  = (String)param.get("startDd");
		String endDd	= (String)param.get("endDd");
		
		if (startDd == null || startDd.equals("")) {
			Map<String,Object> period = installHitMapper.getWeekPeriod();
			
			startDd = (String)period.get("seven");
			endDd	= (String)period.get("today");
		}
		
		param.put("startDd"	, startDd);
		param.put("endDd"	, endDd);
		
		
		List<Map<String,Object>> all 	= null;
		
		if (prdCd.equals("D")) {
			all 	= installHitMapper.getDays(param);
		}
		else if (prdCd.equals("W")) {
			all 	= installHitMapper.getWeeks(param);
		}
		else if (prdCd.equals("M")) {
			all	 	= installHitMapper.getMonths(param);
		}
		else if (prdCd.equals("Y")) {
			all		= installHitMapper.getYears(param);
		}
		
		model.addAttribute("date", all);
				
		return new ModelAndView("excelView", "all", all);
	}
	
	@RequestMapping(value="/admin/getbasicstc.do")
	public String getBasicStatics(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		
		if (log.isDebugEnabled()){
			log.debug("#getBasicStatics.do.....");
		}
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id = userDetails.getUsername();
		
		param.put("chgId", id);
		
		//setting
		Map<String,Object> setting = settingMapper.getSetting(param);
		
//		String prdCd = (String)setting.get("prdCd");
		String item = (String)setting.get("item");
		
		String[] items = item.split("[|]");
		
		String prdCd = (String)param.get("priod");
		
		if (prdCd == null || prdCd.equals("")) {
			prdCd = (String)setting.get("prdCd");
		}
		
		log.debug("#prdCd ==>{}" , prdCd);
		log.debug("#item  ==>{}" , item);
		// D , W , M , Y

		
		//기간
		String startDd  = (String)param.get("startDd");
		String endDd	= (String)param.get("endDd");
		
		if (startDd == null || startDd.equals("")) {
			Map<String,Object> period = installHitMapper.getWeekPeriod();
			
			startDd = (String)period.get("seven");
			endDd	= (String)period.get("today");
		}
		
		param.put("startDd"	, startDd);
		param.put("endDd"	, endDd);
		
		//basic
		String 	siteId = "app0000020";		
		param.put("siteId"	, siteId);
		
		
		List<Map<String,Object>> basic 	= null;
		
		if (prdCd.equals("D")) {
			basic 	= installHitMapper.getDays(param);
		}
		else if (prdCd.equals("W")) {
			basic 	= installHitMapper.getWeeks(param);
		}
		else if (prdCd.equals("M")) {
			basic	 	= installHitMapper.getMonths(param);
		}
		else if (prdCd.equals("Y")) {
			basic		= installHitMapper.getYears(param);
		}
				
		model.addAttribute("priod",prdCd);
		model.addAttribute("basic", basic);
		
		model.addAttribute("startDd", startDd);
		model.addAttribute("endDd"	, endDd);	
		
		return "/stc/basicstc";
	}

	@RequestMapping(value="/admin/basicdownloadExcel.do")
	public ModelAndView getBasicDownload(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		
		if (log.isDebugEnabled()){
			log.debug("#getBasicDownload.do.....");
		}
		
		response.setHeader("Content-disposition", "attachment; filename=basic.xls"); //target명을 파일명으로 작성
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id = userDetails.getUsername();
		
		param.put("chgId", id);
		
		//setting
		Map<String,Object> setting = settingMapper.getSetting(param);
		
//		String prdCd = (String)setting.get("prdCd");
		String item = (String)setting.get("item");
		
		String[] items = item.split("[|]");
		
		String prdCd = (String)param.get("priod");
		
		if (prdCd == null || prdCd.equals("")) {
			prdCd = (String)setting.get("prdCd");
		}
		
		log.debug("#prdCd ==>{}" , prdCd);
		log.debug("#item  ==>{}" , item);
		// D , W , M , Y

		
		//기간
		String startDd  = (String)param.get("startDd");
		String endDd	= (String)param.get("endDd");
		
		if (startDd == null || startDd.equals("")) {
			Map<String,Object> period = installHitMapper.getWeekPeriod();
			
			startDd = (String)period.get("seven");
			endDd	= (String)period.get("today");
		}
		
		param.put("startDd"	, startDd);
		param.put("endDd"	, endDd);
		
		//basic
		String 	siteId = "app0000020";		
		param.put("siteId"	, siteId);
		
		
		List<Map<String,Object>> basic 	= null;
		
		if (prdCd.equals("D")) {
			basic 	= installHitMapper.getDays(param);
		}
		else if (prdCd.equals("W")) {
			basic 	= installHitMapper.getWeeks(param);
		}
		else if (prdCd.equals("M")) {
			basic	 	= installHitMapper.getMonths(param);
		}
		else if (prdCd.equals("Y")) {
			basic		= installHitMapper.getYears(param);
		}
		
		model.addAttribute("date", basic);
				
		return new ModelAndView("excelView", "basic", basic);
	}
}
