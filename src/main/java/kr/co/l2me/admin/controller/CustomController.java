package kr.co.l2me.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import egovframework.rte.fdl.cmmn.exception.FdlException;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import kr.co.l2me.admin.mapper.CustomMapper;
import kr.co.l2me.admin.vo.CustomUserDetails;
import kr.co.l2me.admin.vo.Paging;

@Controller
public class CustomController {
	//Logger
	private static final Logger log = LoggerFactory.getLogger(CustomController.class);
	
	@Autowired
	private CustomMapper mapper;
	
	@Autowired
	EgovIdGnrService idServie;
	
	@RequestMapping(value="/admin/cus/customs.do")
	public String getCustoms(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		log.debug("#getCustoms page.......");
		
		int totalCount = mapper.getCustomCnt();
		
		String cPage = (String)param.get("cPage");
		
		int staPos   = 0 ;
		
		if (cPage != null ) {
			staPos = Integer.parseInt(cPage) * 10;
		}
		else{
			cPage = "1";
		}
		
        Paging paging = new Paging();
        paging.setPageNo(Integer.parseInt(cPage));
        paging.setPageSize(10);
        paging.setTotalCount(totalCount);

        param.put("staPos", staPos);
		
		List<Map<String,Object>> customs = mapper.getCustoms(param);
		
		model.addAttribute("customs"	, customs);
		model.addAttribute("paging"	, paging);
        
		return "/cus/custom";
	}
	
	@RequestMapping(value="/admin/cus/pageadd.do")
	public String pageAdd(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		log.debug("#custom pageadd .......");
		
		return "/cus/customadd";
	}
	
	@RequestMapping(value="/admin/cus/setcustom.do")
	@ResponseBody
	public Map<String,Object> setCustom(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		log.debug("#custom setCustom .......");
				
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id			= userDetails.getUsername();
		
		String siteId = null;
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		try {
			siteId = 	idServie.getNextStringId();
		} catch (FdlException e) {
			e.printStackTrace();
			
			result.put("code" , "9999");
			result.put("msg"  , "FAILED");
			
			return result;
		}
		
		log.debug("#sitdId ==>{}",siteId);
		
		param.put("siteId", siteId);
		log.debug("#param==>{}" , param);
		
		int iRlt = mapper.setCustom(param);
		
		if (iRlt > 0 ){
			iRlt = mapper.setCustomSub(param);
			
			result.put("code" , "0000");
			result.put("msg"  , "SUCCESS");
		}
		else {
			result.put("code" , "9999");
			result.put("msg"  , "FAILED");
		}
		
		return result;
	}
	
	@RequestMapping(value="/admin/cus/updatepage.do")
	public String pageUpdate(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		log.debug("#custom pageUpdate .......{}" , param);
		
		Map<String,Object> custom = mapper.getCustom(param);
		model.addAttribute("custom", custom);
		
		return "/cus/customupdate";
	}
	
	@RequestMapping(value="/admin/cus/updatecustom.do")
	@ResponseBody
	public Map<String,Object> updateCustom(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param){
		log.debug("#custom updateCustom ......." + param);
				
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id			= userDetails.getUsername();
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		log.debug("#param==>{}" , param);
		
		int iRlt = mapper.updateCustom(param);
		
		if (iRlt > 0 ){
			iRlt = mapper.updateCustomSub(param);
			
			result.put("code" , "0000");
			result.put("msg"  , "SUCCESS");
		}
		else {
			result.put("code" , "9999");
			result.put("msg"  , "FAILED");
		}
		
		return result;
	}
}
