package kr.co.l2me.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.l2me.admin.mapper.CustomMapper;
import kr.co.l2me.admin.mapper.ManagerMapper;
import kr.co.l2me.admin.vo.CustomUserDetails;
import kr.co.l2me.admin.vo.Paging;
import kr.co.l2me.cmmn.AESUtil;

@Controller
public class ManagerController {
	//Logger
	private static final Logger log = LoggerFactory.getLogger(ManagerController.class);
	
	@Autowired
	private ManagerMapper managerMapper;
	
	@Autowired
	private CustomMapper customMapper;
	
	@RequestMapping(value="/admin/getmanagers.do")
	public String getManager(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) {
		log.debug("#getusers page.......");
		
		int totalCount = managerMapper.getManagerCnt(param);
		
		String cPage = (String)param.get("cPage");
		
		int staPos   = 0 ;
		
		if (cPage != null ) {
			staPos = Integer.parseInt(cPage) * 10;
		}
		else{
			cPage = "1";
		}
		
        Paging paging = new Paging();
        paging.setPageNo(Integer.parseInt(cPage));
        paging.setPageSize(10);
        paging.setTotalCount(totalCount);

        param.put("staPos", staPos);
		
		
		List<Map<String,Object>> managers = managerMapper.getManagers(param);
		model.addAttribute("managers", managers);
		
		return "/acct/manager";
	}
	
	@RequestMapping(value="/admin/setmanager.do")
	@ResponseBody
	public Map<String,Object> setManager(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) throws Exception{
		
		log.debug("#setmanager.do start......");
		log.debug("#param==>{}", param.toString());
		Map<String,Object> result = new HashMap<String,Object>();
		
		AESUtil aesUtil = new AESUtil();
		
		String pwd = aesUtil.encode("1111");
		
		param.put("password", pwd);
		
		int iRlt = managerMapper.setManager(param);
		
		if (iRlt > 0 ) {
			result.put("code"	, "0000");
			result.put("msg"	, "SUCCESS....");
		}
		else {
			result.put("code"	, "9999");
			result.put("msg"	, "fail....");
		}
				
		return result;
	}
	
	@RequestMapping(value="/admin/setinitpw.do")
	@ResponseBody
	public Map<String,Object> setInitPw(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) throws Exception{
		
		log.debug("#setInitPw.do start......");
		log.debug("#param==>{}", param.toString());
		Map<String,Object> result = new HashMap<String,Object>();
		
		AESUtil aesUtil = new AESUtil();
		
		String pwd = aesUtil.encode("1111");
		
		param.put("password", pwd);
		
		int iRlt = managerMapper.updateInitPw(param);
		
		if (iRlt > 0 ) {
			result.put("code"	, "0000");
			result.put("msg"	, "SUCCESS....");
		}
		else {
			result.put("code"	, "9999");
			result.put("msg"	, "fail....");
		}
				
		return result;
	}
	
	@RequestMapping(value="/admin/setrole.do")
	@ResponseBody
	public Map<String,Object> setRole(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) throws Exception{
		
		log.debug("#setrole.do start......");
		log.debug("#param==>{}", param.toString());
		Map<String,Object> result = new HashMap<String,Object>();
				
		param.put("masterYn", "Y");
		
		int iRlt = managerMapper.updateRole(param);
		
		if (iRlt > 0 ) {
			result.put("code"	, "0000");
			result.put("msg"	, "SUCCESS....");
		}
		else {
			result.put("code"	, "9999");
			result.put("msg"	, "fail....");
		}
				
		return result;
	}
	
	@RequestMapping(value="/admin/deletemanager.do")
	@ResponseBody
	public Map<String,Object> deleteManager(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) throws Exception{
		
		log.debug("#deletemanager.do start......");
		log.debug("#param==>{}", param.toString());
		Map<String,Object> result = new HashMap<String,Object>();
						
		int iRlt = managerMapper.delManager(param);
		
		if (iRlt > 0 ) {
			result.put("code"	, "0000");
			result.put("msg"	, "SUCCESS....");
		}
		else {
			result.put("code"	, "9999");
			result.put("msg"	, "fail....");
		}
		return result;
	}
	
	@RequestMapping(value="/admin/getinfo.do")
	public String getInfo(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model) {
		log.debug("#getInfo page.......");
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id = userDetails.getUsername();
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("chgId", id);
		
		Map<String,Object> manager = managerMapper.getManager(param);
		model.addAttribute("manager", manager);
		
		return "/acct/info";
	}
	
	@RequestMapping(value="/admin/setinfo.do")
	@ResponseBody
	public Map<String,Object> setInfo(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) throws Exception{
		
		log.debug("#setInfo.do start......");
		log.debug("#param==>{}", param.toString());
		Map<String,Object> result = new HashMap<String,Object>();
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id = userDetails.getUsername();
		
		param.put("chgId", id);
		
		Map<String,Object> user = managerMapper.getManager(param);
		String password	= (String)user.get("password");
		
		log.debug("#password==>{}" , password);
		
		String pw 			= (String)param.get("pw");
		
		log.debug("#pw==>{}" , pw);
		
		AESUtil aesUtil = new AESUtil();
		String pwd = aesUtil.encode(pw);
		
		log.debug("#pwd==>{}" , pwd);
		
		if (!pwd.equals(password)) {
			result.put("code"	, "9999");
			result.put("msg"	, "현재 비밀번호가 다릅니다.");
			return result;
		}
		
		//
		String newpw 		= (String)param.get("newpw");
		String confirmpw	= (String)param.get("confirmpw");
		
		if (newpw.equals(confirmpw)) {
			pwd = aesUtil.encode(newpw);
			param.put("password", pwd);
		}
		
		String qnaYn = (String)param.get("qnayn");
		if (qnaYn == null ) {
			param.put("qnayn", "N");
		}
		
		int iRlt = managerMapper.updateManager(param);
		
		if (iRlt > 0 ) {
			result.put("code"	, "0000");
			result.put("msg"	, "SUCCESS....");
		}
		else {
			result.put("code"	, "9999");
			result.put("msg"	, "fail....");
		}
				
		return result;
	}
	
	@RequestMapping(value="/admin/acct/pageadd.do")
	public String pageAdd(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model) {
		log.debug("#pageAdd page.......");
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		String id 		= userDetails.getUsername();
		String siteId 	= userDetails.getSiteId();
		
		List<Map<String,Object>> customNms = customMapper.getCustomNm();

		model.addAttribute("customNms"	, customNms);
		model.addAttribute("siteId"		, siteId);
		
		return "/acct/manageradd";
	}
	
	@RequestMapping(value="/admin/acct/getsitemgt.do")
	@ResponseBody
	public Map<String,Object> getSiteManagers(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model 
			, @RequestParam Map<String,Object> param) throws Exception{
		
		log.debug("#getsitemgt.do start......");
		log.debug("#param==>{}", param.toString());
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String id = userDetails.getUsername();
		
		param.put("chgId", id);
		
			
		List<Map<String,Object>> managers = managerMapper.getAppManagers(param);
		
		result.put("managers", managers);
				
		return result;
	}
}
