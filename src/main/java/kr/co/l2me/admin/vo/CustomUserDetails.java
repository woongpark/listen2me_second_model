package kr.co.l2me.admin.vo;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomUserDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1508319374182437949L;
	
	private String username;
	private String password;
	
	private String chgTp;
	private String otpKey;
	
	private String siteId;
	
	private List<GrantedAuthority> authorities ;
	
	public CustomUserDetails(String username, String password) {
		this.username = username;
		this.password = password;
	}

	@Override
	public List<GrantedAuthority> getAuthorities() {      
        return authorities;
	}

	public void setAuthorities(List<GrantedAuthority> authorities){
		this.authorities = authorities;
	}
	
	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public String getChgTp() {
		return chgTp;
	}

	public void setChgTp(String chgTp) {
		this.chgTp = chgTp;
	}

	public String getOtpKey() {
		return otpKey;
	}

	public void setOtpKey(String otpKey) {
		this.otpKey = otpKey;
	}

	public boolean isMaster(){
		GrantedAuthority master = new SimpleGrantedAuthority("ROLE_MASTER");
		for ( GrantedAuthority simple : authorities) {
			if (simple.equals(master))
				return true;
		}
		return false;
	}
	
	public boolean isSubMaster(){
		GrantedAuthority master = new SimpleGrantedAuthority("ROLE_SUB_MASTER");
		for ( GrantedAuthority simple : authorities) {
			if (simple.equals(master))
				return true;
		}
		return false;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	@Override
	public String toString() {
		return "CustomUserDetails [username=" + username + ", password=" + password + ", chgTp=" + chgTp + ", otpKey="
				+ otpKey + ", siteId=" + siteId + ", authorities=" + authorities + "]";
	}
	
}
