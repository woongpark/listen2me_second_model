package kr.co.l2me.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.l2me.admin.mapper.CommonMapper;
import kr.co.l2me.admin.mapper.MainMapper;
import kr.co.l2me.admin.mapper.ManagerMapper;
import kr.co.l2me.admin.mapper.QnaMapper;
import kr.co.l2me.admin.mapper.ReportMapper;
import kr.co.l2me.admin.mapper.SettingMapper;
import kr.co.l2me.admin.vo.CustomUserDetails;
import kr.co.l2me.admin.vo.Paging;
import kr.co.l2me.cmmn.OtpUtil;

@Controller
public class MainController {
	private static final Logger LOG = LoggerFactory.getLogger(MainController.class);
	
	@Autowired
	CommonMapper 	commonMapper;
	
	@Autowired
	MainMapper 		mainMapper;
	
	@Autowired
	ReportMapper	reportMapper;
	
	@Autowired
	ManagerMapper	managerMapper;
	
	@Autowired
	QnaMapper		qnaMapper;
	
	@Autowired
	SettingMapper	settingMapper;
	
	@RequestMapping(value={"/","/login.do"})
	public String login(){
		LOG.debug("#login Page...");
		return "/login";
	}
	
	@RequestMapping(value="/logouting.do")
	public String logouting(){
		LOG.debug("#logouting Page...");
		return "redirect:/logout.do";
	}
	
	@RequestMapping(value="/setlogin.do")
	public String setLogin(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model){
		LOG.debug("#setLogin.do...............");
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		LOG.debug("#detail==>{}",userDetails.toString());
		session.setAttribute("login", userDetails);
				
		return "redirect:/admin/setotp.do";
	}
	
	@RequestMapping(value="/admin/setotp.do")
	public String setOtp(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model){
		LOG.debug("#setotp.do...............");
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		String chgId	= userDetails.getUsername();
		String otpKey 	= userDetails.getOtpKey();
		
		if ("N".equals(otpKey)) {
			OtpUtil otp = new OtpUtil();
			
			Map<String,String> map = otp.getEncodedKey(userDetails.getUsername());
			
			otpKey = (String)map.get("encodedKey");
			
			model.addAttribute("encodedKey"	, otpKey );
			model.addAttribute("url"		, (String)map.get("url"));
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("chgId"	, chgId);
			param.put("otpKey"	, (String)map.get("encodedKey"));
			
			managerMapper.updateManager(param);
		}
		
		return "/otp";
	}
	
	@RequestMapping(value="/admin/verifyotp.do")
	public String verifyOtp(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model , 
			@RequestParam Map<String,Object> param){
		LOG.debug("#verifyOtp.do...............{}" , param);
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		
		String chgId	= userDetails.getUsername();
		String otpKey 	= userDetails.getOtpKey();
		
		String pin		= (String)param.get("pin");
		
		if ( !"N".equals(otpKey)) {
			OtpUtil otp = new OtpUtil();
			
			try {
//				boolean bRlt = otp.checkCode(otpKey, pin);
				
				boolean bRlt = true;
				
				LOG.debug("#bRlt ==>{}", bRlt);
				
				if (bRlt) {
										
					if (userDetails.isMaster()) {
						LOG.debug("#userDetails.isMaster()==>{}" , userDetails.isMaster());
//						return "redirect:/admin/cus/customs.do";
						return "redirect:/admin/main.do";
					} else if (userDetails.isSubMaster()) {
						LOG.debug("#userDetails.isSubMaster()==>{}" , userDetails.isSubMaster());
						return "redirect:/admin/main.do";
					} else {
						return "redirect:/admin/main.do";
					}
				}
				else {
					model.addAttribute("otpYn", "N");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "redirect:/admin/login.do";
			}
		}
		
		return "otp";
	}
	
	@RequestMapping(value="/admin/main.do")
	public String main(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model
			, @RequestParam Map<String,Object> param ){
		LOG.debug("#Main Page...:{}" , commonMapper.getCurrentTime());
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();
		String chgTp = userDetails.getChgTp();
		String chgId = userDetails.getUsername();
		
		int userCnt 	= mainMapper.selectUserCnt(null);
		int reportCnt	= mainMapper.selectReportCnt(null);
		int confirmCnt	= mainMapper.selectConfirmCnt(null);
		int serveyCnt	= mainMapper.selectServeyCnt(null);
		int waitCnt		= mainMapper.selectWaitCnt(null);
		int finishCnt	= mainMapper.selectFinishCnt(null);
		
		
		model.addAttribute("userCnt"	, userCnt);
		model.addAttribute("reportCnt"	, reportCnt);
		model.addAttribute("confirmCnt"	, confirmCnt);
		model.addAttribute("serveyCnt"	, serveyCnt);
		model.addAttribute("waitCnt"	, waitCnt);
		model.addAttribute("finishCnt"	, finishCnt);
		
		//문의사항 
		int totalCount = qnaMapper.getQnaCnt(param);
		
		String cPage = (String)param.get("cPage");
		
		int staPos   = 0 ;
		
		if (cPage != null ) {
			staPos = Integer.parseInt(cPage) * 10;
		}
		else{
			cPage = "1";
		}
		
        Paging paging = new Paging();
        paging.setPageNo(Integer.parseInt(cPage));
        paging.setPageSize(10);
        paging.setTotalCount(totalCount);

        param.put("staPos", staPos);
        
        //
        String siteCd = null;
        		
        if ( !StringUtils.isEmpty((String)param.get("siteCd")) )
        	siteCd = (String)param.get("siteCd");
		
		List<Map<String,Object>> qnas = qnaMapper.getQnas(param);
		
		model.addAttribute("qnas"	, qnas);
		model.addAttribute("paging"	, paging);
		model.addAttribute("siteCd"	, siteCd);
		
		//setting 
		param.put("chgId", chgId);
		Map<String,Object> setting = settingMapper.getSetting(param);
		
		setting = makeSetting(setting);
		
		model.addAttribute("setting", setting);
		
		if (!chgTp.equals("000")) {
			return "/main";
		} else {
			return "/main_m";
		}
	}
	
	@RequestMapping(value="/findcharge.do")
	public String getCharge(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model){
		LOG.debug("#findcharge.do...............");	
		return "/charge";
	}
	
	@RequestMapping(value="/getorg.do")
	@ResponseBody
	public Map<String,Object> getOrg(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model
			, @RequestParam Map<String,Object> param ){
		LOG.debug("#getOrg.do...............");
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("org", "listen2me.com");
		
		return result;
	}
	
	@RequestMapping(value="/setsetting.do")
	@ResponseBody
	public Map<String,Object> setSetting(HttpServletRequest request , HttpServletResponse response , HttpSession session , Model model
			, @RequestParam Map<String,Object> param ){
		LOG.debug("#setSetting.do...............{}" , param);
		
		CustomUserDetails userDetails = (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();

		String chgId = userDetails.getUsername();
		param.put("chgId", chgId);
		
		Map<String,Object> setting = settingMapper.getSetting(param);
		
		int iRlt = 0 ;
		Map<String,Object> result = new HashMap<String,Object>();
		
		if (setting == null) {
			iRlt = settingMapper.setSetting(param);
		} else {
			iRlt = settingMapper.updateSetting(param);
		}
		
		if (iRlt > 0) {
			result.put("code", "0000");
		} else {
			result.put("code", "9999");
		}
		
		return result;
	}
	
	
	
	public Map<String,Object> makeSetting(Map<String,Object> map){
		
		if ( map == null ) {
			map = new HashMap<String,Object>();
		}
		
		String 		prdCd 	= (String)map.get("prdCd");
		String 		item 	= (String)map.get("item");
		
		String[] 	items	= null;
		
		
		if (prdCd == null ) {
			prdCd 	= "M";
			map.put("prdCd", prdCd);
		}
			
		if ( item == null ) {
			items	= new String[5];
			
			items[0] = "all";
			items[1] = "basic";
			items[2] = "cnx";
			items[3] = "record";
			items[4] = "common";			
		}else {
			items	= item.split("[|]");			
		}
			
		map.put("items", items);
			
		return map;
	}
}
