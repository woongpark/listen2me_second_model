package kr.co.l2me.admin.auth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import kr.co.l2me.admin.mapper.CommonMapper;
import kr.co.l2me.admin.mapper.ManagerMapper;
import kr.co.l2me.admin.vo.CustomUserDetails;
import kr.co.l2me.cmmn.AESUtil;

public class UserAuthenticationProvider implements AuthenticationProvider {

	private static final Logger log = LoggerFactory.getLogger(UserAuthenticationProvider.class);
	
	@Autowired
	CommonMapper commonMapper;
	
	@Autowired
	ManagerMapper managerMapper;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String id 		= (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();

		log.debug("#UserAuthenticationProvider==>{},{}" , id , password);
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("chgId", id);
		
		Map<String,Object> manager = managerMapper.getManager(param);
		
		if (manager == null ) {
			// 실패시 예외처리
			throw new BadCredentialsException("존재하지 않는 사용자 입니다.");
		}
		
		String lockYn = (String)manager.get("lockYn");
		
		//락킹 유무
		if (lockYn.equals("Y")) {
			throw new LockedException("사용자 계정이 잠겨있습니다. 관리자에게 문의해주세요.");
		}
		
		//패스워드 검증
		AESUtil aesUtil 	= new AESUtil();
		String cipherText	= null;
		try {
			cipherText = aesUtil.encode(password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("",e);
		}
		
		String 	encodedText 	= (String)manager.get("password");
		
		String 	sErrCn		= (String)manager.get("errCn");
		
		int 	iErrCn		= Integer.parseInt(sErrCn);
		
		//site id
		String siteId 		= (String)manager.get("appId");
		
		if (iErrCn > 5) {
			param.put("lockYn"	, "Y");
			param.put("errCn"	, iErrCn++ );
			managerMapper.updateErrCnt(param);
			
			throw new LockedException("사용자 계정이 잠겨있습니다. 관리자에게 문의해주세요.");
		}
				
		//아이디 , 패스워드 검증
		if(!encodedText.equals(cipherText)){
			param.put("errCn"	, ++iErrCn );
			managerMapper.updateErrCnt(param);
			// 실패시 예외처리
			throw new BadCredentialsException("아이디 또는 비밀번호가 일치하지 않습니다.</br> 암호 오류 횟수" + iErrCn + "/5 </br> 5회 초과 시 담당자에게 문의하셔야 합니다.");
		}
		else {		
			param.put("errCn"	, 0 );
			managerMapper.updateErrCnt(param);
			
			List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
			roles.add(new SimpleGrantedAuthority("ROLE_USER"));
			
			String chgTp = (String)manager.get("chgTp");
			
			if ("000".equals(chgTp)) {
				roles.add(new SimpleGrantedAuthority("ROLE_MASTER"));
			}
			
			if ("001".equals(chgTp)) {
				roles.add(new SimpleGrantedAuthority("ROLE_SUB_MASTER"));
			}
				
			String otpKey = (String)manager.get("otpKey");
			
			UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(id, password, roles);
			
			CustomUserDetails userDetails = new CustomUserDetails(id, password);
			userDetails.setAuthorities(roles);
			
			userDetails.setChgTp(chgTp);
			userDetails.setOtpKey(otpKey);
			
			userDetails.setSiteId(siteId);
						
			result.setDetails(userDetails);
			
			log.debug("#==>{}",result);
			
			return result;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		// TODO Auto-generated method stub
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
