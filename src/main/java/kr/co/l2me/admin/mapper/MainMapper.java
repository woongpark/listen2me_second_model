package kr.co.l2me.admin.mapper;

import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper
public interface MainMapper {
	//사용자/설치 수
	public int selectUserCnt(Map<String,Object> param);
	//신고 건수
	public int selectReportCnt(Map<String,Object> param);
	//확인 건수
	public int selectConfirmCnt(Map<String,Object> param);
	//조사 건수
	public int selectServeyCnt(Map<String,Object> param);
	//대기
	public int selectWaitCnt(Map<String,Object> param);
	//조치/완료
	public int selectFinishCnt(Map<String,Object> param);
}
