package kr.co.l2me.admin.mapper;

import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper
public interface ManagerMapper {
	//
	public List<Map<String,Object>> getManagers(Map<String,Object> param);
	//
	public Map<String,Object> getManager(Map<String,Object> param);
	//
	public int setManager(Map<String,Object> param);
	//
	public int updateErrCnt(Map<String,Object> param);
	//
	public int updateManager(Map<String,Object> param);
	//
	public int updateInitPw(Map<String,Object> param);
	//
	public int updateRole(Map<String,Object> param);
	//
	public int delManager(Map<String,Object> param);
	//
	public int getManagerCnt(Map<String,Object> param);
	//
	public List<Map<String,Object>> getAppManagers(Map<String,Object> param);
}
