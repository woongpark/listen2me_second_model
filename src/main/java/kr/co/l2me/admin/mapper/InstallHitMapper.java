package kr.co.l2me.admin.mapper;

import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper
public interface InstallHitMapper {
	//
	public List<Map<String,Object>> getDayWeeks(Map<String,Object> param);
	//
	public List<Map<String,Object>> getDays(Map<String,Object> param);
	//
	public List<Map<String,Object>> getWeeks(Map<String,Object> param);
	//
	public List<Map<String,Object>> getMonths(Map<String,Object> param);
	//
	public List<Map<String,Object>> getYears(Map<String,Object> param);
	//
	public Map<String,Object> getWeekPeriod();
}
