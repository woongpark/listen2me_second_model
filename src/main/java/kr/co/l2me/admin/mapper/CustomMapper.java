package kr.co.l2me.admin.mapper;

import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper
public interface CustomMapper {
	//
	public List<Map<String,Object>> getCustoms(Map<String,Object> param);
	//
	public int getCustomCnt();
	//
	public Map<String,Object> getCustom(Map<String,Object> param);
	//
	public int setCustom(Map<String,Object> param);
	//
	public int setCustomSub(Map<String,Object> param);
	//
	public int deleteCustom(Map<String,Object> param);
	//
	public int updateCustom(Map<String,Object> param);
	//
	public int updateCustomSub(Map<String,Object> param);
	//
	public List<Map<String,Object>> getCustomNm();
}
