package kr.co.l2me.admin.mapper;

import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper
public interface BoardMapper {
	//
	public List<Map<String,Object>> getBoards(Map<String,Object> param);
	//
	public int getBoardCnt();
	//
	public Map<String,Object> getBoard(Map<String,Object> param);
	//
	public int setBoard(Map<String,Object> param);
	//
	public int deleteBoard(Map<String,Object> param);
	//
	public int updateBoard(Map<String,Object> param);
}
