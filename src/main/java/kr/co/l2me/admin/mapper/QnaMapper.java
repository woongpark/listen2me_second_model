package kr.co.l2me.admin.mapper;

import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper
public interface QnaMapper {
	//
	public List<Map<String,Object>> getQnas(Map<String,Object> param);
	//
	public int getQnaCnt(Map<String,Object> param);
	//
	public Map<String,Object> getQna(Map<String,Object> param);
	//
	public int setQna(Map<String,Object> param);
	//
	public int deleteQna(Map<String,Object> param);
	//
	public int updateQna(Map<String,Object> param);
	//
	public int updateQnaAns(Map<String,Object> param);
}
