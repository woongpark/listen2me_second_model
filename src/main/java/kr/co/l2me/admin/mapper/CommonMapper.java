package kr.co.l2me.admin.mapper;

import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper
public interface CommonMapper {
	//
	public String getCurrentTime();
	//
	public Map<String,Object> getKeys(Map<String,Object> param);
	//
	public Map<String,Object> getAttacker(Map<String,Object> param);
}
