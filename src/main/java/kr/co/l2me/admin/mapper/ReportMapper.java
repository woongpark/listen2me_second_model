package kr.co.l2me.admin.mapper;

import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper
public interface ReportMapper {
	//
	public List<Map<String,Object>> getReports(Map<String,Object> param);
	//
	public int	getReportCnt();
	//
	public List<Map<String,Object>> getReportDetails(Map<String,Object> param);
	//
	public int setReportDtail(Map<String,Object> param);
	//
	public int updateReportDetail(Map<String,Object> param);
	//
	public List<Map<String,Object>> getReportMain(Map<String,Object> param);
	//
	public int getReportStatusCnt(Map<String,Object> param);
}
