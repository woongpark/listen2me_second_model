package kr.co.l2me.admin.mapper;

import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper
public interface SettingMapper {
	//
	public Map<String,Object> getSetting(Map<String,Object> param);
	//
	public int setSetting(Map<String,Object> param);
	//
	public int updateSetting(Map<String,Object> param);
}
