<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>
<script type="text/javascript">

function fn_selected(reportSeq){
// 	$(location).attr("href" , '<c:url value="/admin/getreport.do"/>?reportSeq='+reportSeq);
	$(location).attr("href" , '<c:url value="/admin/getreportdetail.do"/>?reportSeq='+reportSeq);
}

function goPage(cpage){
	$(location).attr("href" , '<c:url value="/admin/getreports.do"/>?cPage='+cpage);
}

</script>
</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->
    
<!-- content -->    
<section id="content">
	<main class="main">
		<div class="clearfix">
	        <h4><i class="icon-bullhorn"></i>리포트 목록</h4>
	    </div>
		<section class="panel text-small">
			<div class="row-fluid m-t-small">
	        	<div class="span12">

					<div class="pull-out m-t-small">
			             <table class="table table-striped b-t text-small">
							<thead>
								<tr>
									<th style="text-align:center;" width="10%">No</th>
			                    	<th style="text-align:center;" width="30%">사건명 + 구분</th>                    
			                    	<th style="text-align:center;" width="10%">상태</th>
			                    	<th style="text-align:center;" width="20%">발생일/신고일</th>
			                    	<th style="text-align:center;" width="10%">행위자 ID</th>
			                    	<th style="text-align:center;" width="10%">제보자 ID</th>
			                  </tr>
			                 </thead>
							<tbody>
							<c:choose>
								<c:when test="${not empty reports}">
									<c:forEach items="${reports}" var="report" varStatus="status">
										<tr>
											<td style="text-align:center;">${status.count}</td>
											<td onclick="fn_selected('${report.reportSeq}');">
												${report.title}
												<c:if test="${empty report.title}">${report.rgsDt } 사건 접수</c:if>
												<c:if test="${report.actYn eq 'N'}"><i class="icon-comment"></i></c:if>
												<c:if test="${report.actYn eq 'Y'}"><i class="icon-comments-alt"></i></c:if>
											</td>
											<td style="text-align:center;">
												<c:choose>
													<c:when test="${report.reportStatus eq 'R'}">이용자신고</c:when>
													<c:when test="${report.reportStatus eq 'C'}">신고확인</c:when>
													<c:when test="${report.reportStatus eq 'I'}">사건접수/조사중</c:when>
													<c:when test="${report.reportStatus eq 'W'}">처분대기</c:when>
													<c:when test="${report.reportStatus eq 'F'}">완결</c:when>
												</c:choose>
											</td>
											<td style="text-align:center;"> ${report.occurDt}/${report.rgsDt }</td>
											<td style="text-align:center;">
												<fmt:formatNumber value="${report.reportSeq}" pattern="00000"/>
											</td>
											<td style="text-align:center;">
												<fmt:formatNumber value="${report.userSeq}" pattern="00000"/>
											</td>
										</tr>
									</c:forEach>					
								</c:when>
								<c:otherwise>
									<tr class="text-center">
										<td colspan="4" style="text-align:center;">데이터가 없습니다.</td>
									</tr>					
								</c:otherwise>
							</c:choose>		
							
							<tr class="text-center">
								<td colspan="6" style="text-align:center;"></td>
							</tr>					
									
							</tbody>
						</table>
					</div>
	        	
	        		<!-- paging -->
	        		
					<div class="clearfix">
							<div class="span12">
								<div class="span6">
									<i class="icon-exclamation-sign"></i>공동알림
									<i class="icon-comment"></i>단독신고
									<i class="icon-comments-alt"></i>공동신고
								</div>
								<div class="span6">
					                <div class="text-right text-center-sm" align="right">
										<div class="pagination m-b-none">    
											<ul>
												<li><a href="goPage(${paging.prevPageNo});">&nbsp;<i class="icon-chevron-left"></i></a></li>
												
										        <c:forEach var="i" begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1">
										            <c:choose>
										                <c:when test="${i eq paging.pageNo}">
										                	<li><a href="javascript:goPage(${i})" class="choice">${i}</a></li>
										                </c:when>
										                <c:otherwise>
										                	<li><a href="javascript:goPage(${i})">${i}</a></li>
										                </c:otherwise>
										            </c:choose>
										        </c:forEach>						
												<li><a href="goPage(${paging.nextPageNo});">&nbsp;<i class="icon-chevron-right"></i></a></li>
											</ul>
										</div>
					                </div>
					        	</div>
					                
			                </div>
					    </div>	        		
	        		
	        		<!-- paging -->
				</div>
			</div>
		</section>
	</main>
</section>
<!-- content -->

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->

</body>
</html>

