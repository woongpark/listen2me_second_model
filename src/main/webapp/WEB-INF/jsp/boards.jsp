<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">
function fn_addpage(){
	$(location).attr('href' , '<c:url value="/admin/addboard.do"/>');
}

function fn_selected(boardId){
	$(location).attr('href' , '<c:url value="/admin/updatepage.do"/>?boardId='+boardId);
}

function fn_remove(){
	
	var cnt = $("input:checkbox[name='boardId']:checked").length;
	
	if (cnt == 0 ) {
		alert('선택된 공지사항이 없습니다.');
		return;
	}
	
	if (cnt > 1) {
		alert('하나만 선택 가능합니다.');
		return ;
	}
	
	var id = $("input:checkbox[name='boardId']:checked").val();
	var param = "boardId=" + id;
	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/admin/deleteboard.do"/>'
		,success : function(data){
			if( data.code == '0000' ){
				alert("공지사항를 삭제하였습니다.");
				$(location).attr("href" , '<c:url value="/admin/getboards.do"/>');
			}else{
				alert("시스템 오류가 발생했습니다.");
			}
		}
	});
}

function goPage(cpage){
	$(location).attr("href" , '<c:url value="/admin/getboards.do"/>?cPage='+cpage);
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->
    
<!-- content -->    
<section id="content">
	<main class="main">
		
		<div class="clearfix">
	        <h4><i class="icon-edit"></i>공지관리</h4>
	    </div>
		<section class="panel text-small">
			
			<div class="row-fluid m-t-small">
	        	<div class="span12">
			
					<div class="clearfix">
						<div class="span12" align="right">
			           		<a href="javascript:fn_addpage();" class="btn btn-default btn-small">글쓰기</a>
			           	</div>
			        </div>

				<div class="pull-out m-t-small">
						
	              <table class="table table-striped b-t text-small">
					<thead>
						<tr>
							<th style="text-align:center;" width="5%"></th>
							<th style="text-align:center;" width="5%">No</th>
	                    	<th style="text-align:center;" width="60%">제목</th>                    
	                    	<th style="text-align:center;">출력유무</th>
	                    	<th style="text-align:center;">등록일자</th>
	                  </tr>
	                 </thead>
					<tbody>
					<c:choose>
						<c:when test="${not empty boards}">
							<c:forEach items="${boards}" var="board" varStatus="status">
								<tr>
									<td style="text-align:center;">
				                     	<div class="checkbox">
					                        <label>
					                          <input type="checkbox" name="boardId" id="boardId"  value="${board.boardId}">
					                        </label>
				                      	</div>
									</td>
									<td style="text-align:center;">${status.count}</td>
									<td onclick="fn_selected('${board.boardId}');">${board.title }</td>
									<td style="text-align:center;">${board.displayYn}</td>
									<td style="text-align:center;">${board.rgsDt }</td>
								</tr>
							</c:forEach>					
						</c:when>
						<c:otherwise>
							<tr class="text-center">
								<td colspan="5" style="text-align:center;">데이터가 없습니다.</td>
							</tr>					
						</c:otherwise>
					</c:choose>		
					
					<tr class="text-center">
						<td colspan="5" style="text-align:center;"></td>
					</tr>					
							
					</tbody>
					</table>
						       		       		
			</div>
			
			<div class="clearfix">
					<div class="span12">
			          	<div class="span1" align="right">
			           		<a href="javascript:fn_remove();" class="btn btn-default btn-small">삭제</a>
			           	</div>	
		                <div class="span11 text-right text-center-sm" align="right">
							<div class="pagination pagination-small  m-b-none">    
								<ul>
									<li><a href="goPage(${paging.prevPageNo});">&nbsp;<i class="icon-chevron-left"></i></a></li>
									
							        <c:forEach var="i" begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1">
							            <c:choose>
							                <c:when test="${i eq paging.pageNo}">
							                	<li><a href="javascript:goPage(${i})" class="choice">${i}</a></li>
							                </c:when>
							                <c:otherwise>
							                	<li><a href="javascript:goPage(${i})">${i}</a></li>
							                </c:otherwise>
							            </c:choose>
							        </c:forEach>						
									<li><a href="goPage(${paging.nextPageNo});">&nbsp;<i class="icon-chevron-right"></i></a></li>
								</ul>
							</div>
		                </div>
	                </div>
			    </div>		
						
			</div>
			</div>
		</section>
	</main>
</section>
<!-- content -->

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->

</body>
</html>

