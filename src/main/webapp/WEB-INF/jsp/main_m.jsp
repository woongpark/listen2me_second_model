<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<style>
#overlay_t { background-color: #000; bottom: 0; left: 0; opacity: 0.5; filter: alpha(opacity = 50); /* IE7 & 8 */ position: fixed; right: 0; top: 0; z-index: 99; display:none;}
#popup_layer { width:1000px; margin-bottom:100px; background:#fff; border:solid 1px #ccc; position:absolute; top:260px; left:35%; margin-left:-200px; box-shadow: 0px 1px 20px #333; z-index:100; display:none;}
</style>

<script type="text/javascript">

$( document ).ready(function() { 
	
	$("d").attr("checked",true);
	
    $('.trigger').click(function(){ 
     $('#popup_layer, #overlay_t').show(); 
     $('#popup_layer').css("top", Math.max(0, $(window).scrollTop() + 100) + "px"); 
//      $('#popup_layer').css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px"); 

     $( "#sortable" ).sortable();
     $( "#sortable" ).disableSelection();
     
 }); 
 $('#overlay_t, .close').click(function(e){ 
     e.preventDefault(); 
     $('#popup_layer, #overlay_t').hide(); 
 }); 
});


function goPage(cpage){
	var siteCd = $("#siteCd").val();
	
	var param = 'cPage=' + cpage + '&siteCd=' +siteCd
	
	$(location).attr("href" , '<c:url value="/admin/main.do"/>?'+param);
}

function fn_selected(reportSeq){
	$(location).attr("href" , '<c:url value="/admin/getreportdetail.do"/>?reportSeq='+reportSeq);
}

function fn_setting(){
	alert('setting...');
}

function fn_search(){
	var srh = $("#siteCd").val();
	alert(srh);
}

function fn_cancel(){
    $('#popup_layer, #overlay_t').hide(); 
}

function fn_apply(){
	$('#popup_layer, #overlay_t').hide(); 
	
	var item=""  , id = "";
	
    $("#sortable .span2").each(function(idx , obj){
//    		console.log( "##idx==>" + idx + "==" + this.id);
   		id = this.id + "|";
   		item += id;
    });
    console.log("==>" + item);
    
    var prdCd = $("input:radio[name=priod]:checked").val();
    console.log( prdCd );
    
    var param = "prdCd="+prdCd+"&item="+item;
    
	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/setsetting.do"/>'
		,success : function(data){
			if( data.code == '0000' ){
				$(location).attr("href" , '<c:url value="/admin/main.do"/>');
			}else{
				alert("시스템 오류가 발생했습니다.");
			}
		}
	});
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->

<!-- content -->    
<section id="content">
	<main class="main">

	<br>
	<section class="panel text-small">
		<header class="panel-heading">
			<div class="span2">Dashboard</div>
			<p align="right"><a href="#" class="trigger"><i class="icon-cog"></i></a></p>
		</header>
			
		<div class="row-fluid m-t-small">
			<div class="span12">
			        
				<div class="row-fluid row-1-phone">	  
					  <c:choose>
						<c:when test="${not empty setting.items}">
							<c:forEach items="${setting.items}" var="item">
			
						            <!-- easypiechart -->
						            
						            <c:if test="${item eq 'all'}">
							            <div class="span2">
							              <section class="panel">
							              	<header class="panel-heading bg-white">
												<div class="text-center h5"><i class="icon-barcode"></i><strong>설치(전체)</strong></div>
							                </header>
							                <div class="pull-in text-center">
							                  <div class="inline">
							                      <span class="h2">${userCnt}</span>
							                  </div>
							                </div>
							              </section>
										</div>
									</c:if>
										
									<c:if test="${item eq 'basic'}">
										<div class="span2">
							              <section class="panel">
							              	<header class="panel-heading bg-white">
												<div class="text-center h5"><i class="icon-bolt"></i><strong>설치(BASIC)</strong></div>
							                </header>
							                <div class="pull-in text-center">
							                  <div class="inline">
							                      <span class="h2">${userCnt}</span>
							                  </div>
							                </div>
							              </section>			
										</div>
									</c:if>
									
									<c:if test="${item eq 'cnx'}">
										<div class="span2">
							              <section class="panel">
							              	<header class="panel-heading bg-white">
												<div class="text-center h5"><i class="icon-lock"></i><strong>설치(CNX)</strong></div>
							                </header>
							                <div class="pull-in text-center">
							                  <div class="inline">
							                      <span class="h2">${reportCnt}</span>
							                  </div>
							                </div>
							              </section>			
										</div>
									</c:if>
									
									<c:if test="${item eq 'record'}">
										<div class="span2">
							              <section class="panel">
							              	<header class="panel-heading bg-white">
												<div class="text-center h5"><i class="icon-off"></i><strong>기록건수</strong></div>
							                </header>
							                <div class="pull-in text-center">
							                  <div class="inline">
							                      <span class="h2">${confirmCnt}</span>
							                  </div>
							                </div>
							              </section>			
										</div>
									</c:if>	
										
									<c:if test="${item eq 'common'}">
							            <div class="span2">              
							              <section class="panel">
							              	<header class="panel-heading bg-white">
							              	<div class="text-center h5"><i class="icon-link"></i><string>공동알림건수</string></div>
							                </header>
							                <div class="pull-in text-center">
							                  <div class="inline">
							                      <span class="h2">${serveyCnt}</span>
							                  </div>
							                </div>
							              </section>
										</div>
									</c:if>
							
			<!-- 				<div class="span2"> -->
			<!-- 	              <section class="panel"> -->
			<!-- 	              	<header class="panel-heading bg-white"> -->
			<!-- 						<div class="text-center h5"><i class="icon-ok"></i><strong>완료건수</strong></div> -->
			<!-- 	                </header> -->
			<!-- 	                <div class="pull-in text-center"> -->
			<!-- 	                  <div class="inline"> -->
			<%-- 	                      <span class="h2">${finishCnt}</span> --%>
			<!-- 	                  </div> -->
			<!-- 	                </div> -->
			<!-- 	              </section>			 -->
			<!-- 				</div> -->				
				            <!-- easypiechart end-->
				            
		            		</c:forEach>
		        		</c:when>
			  		</c:choose>	            
	            
	          </div>
			</div>
		</div>
	</section>

	<br>
	
	<section class="panel text-small">'
		<div class="row-fluid">
		
<!-- 			<section class="panel"> -->
<!-- 				<div class="row-fluid"> -->
				
<!-- 			            <div class="span2"> -->
<!-- 			              <section class="panel"> -->
<!-- 			              	<header class="panel-heading bg-white"> -->
<!-- 								<div class="h5"><strong>고객사별 현황</strong></div> -->
<!-- 			                </header> -->
<!-- 			                <div class="pull-in text-center"> -->
<!-- 			                  <div class="inline"> -->
<!-- 			                      <span class="h5">전체</span><br> -->
<!-- 			                      <span class="h5">기업</span><br> -->
<!-- 			                      <span class="h5">학교</span><br> -->
<!-- 			                      <span class="h5">공공</span> -->
<!-- 			                  </div> -->
<!-- 			                </div> -->
<!-- 			              </section> -->
<!-- 						</div>		 -->
						
			            <div class="span12">
			              <section class="panel">
			              	<header class="panel-heading bg-white">
								<div class="h5"><strong>문의사항</strong></div>
								<div class="h5 pull-right">
									고객타입
									<select name="siteCd" id="siteCd" onchange="fn_search();">
										<option value="" 	<c:if test="${empty siteCd}">selected="selected"</c:if>>전체</option>
										<option value="01"  <c:if test="${siteCd eq '01'}">selected="selected"</c:if>>기업</option>
										<option value="02"  <c:if test="${siteCd eq '02'}">selected="selected"</c:if>>학교</option>
										<option value="03"  <c:if test="${siteCd eq '03'}">selected="selected"</c:if>>공공</option>
									</select>
								</div>
			                </header>
			                <div class="pull-in text-center">
								<table class="table table-striped m-b-none text-small">
										<thead>
											<tr>
												<th style="text-align:center;" width="20%">고객사</th>
						                    	<th style="text-align:center;" width="50%">문의제목</th>                    
						                    	<th style="text-align:center;">문의일</th>
						                    	<th style="text-align:center;">처리여부</th>
						                  </tr>
						                 </thead>
										<tbody>
										<c:choose>
											<c:when test="${not empty qnas }">
												<c:forEach items="${qnas}" var="qna" varStatus="status">
													<tr onclick="fn_selected('${qna.qnaId}');">
														<td>
															${qna.siteNm}							
														</td>
														<td style="text-align:center;">
															${qna.title}
														</td>
														<td style="text-align:center;">${qns.rgsDt}</td>
														<td style="text-align:center;">
															<c:choose>
																<c:when test="${qna.procStu eq '00' }">접수</c:when>
																<c:when test="${qna.procStu eq '01' }">답변</c:when>
															</c:choose>
														</td>
													</tr>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<tr>
													<td style="text-align:center;" colspan="4">데이터가 없습니다.</td>
												</tr>					
											</c:otherwise>
										</c:choose>
											<tr>
												<td colspan="4"></td>
											</tr>							
										</tbody>
									</table>
			                </div>
			                
							<div class="clearfix">
								<div class="span12">
					                <div class="span12 text-right text-center-sm" align="right">
										<c:if test="${not empty qnas}">
											<div class="pagination pagination-small  m-b-none">    
												<ul>
													<li><a href="goPage(${paging.prevPageNo});">&nbsp;<i class="icon-chevron-left"></i></a></li>
													
											        <c:forEach var="i" begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1">
											            <c:choose>
											                <c:when test="${i eq paging.pageNo}">
											                	<li><a href="javascript:goPage(${i})" class="choice">${i}</a></li>
											                </c:when>
											                <c:otherwise>
											                	<li><a href="javascript:goPage(${i})">${i}</a></li>
											                </c:otherwise>
											            </c:choose>
											        </c:forEach>						
													<li><a href="goPage(${paging.nextPageNo});">&nbsp;<i class="icon-chevron-right"></i></a></li>
												</ul>
											</div>
										</c:if>
					                </div>
					        	</div>
					        </div>			                
			                
			                
			              </section>
						</div>		
				
					
<!-- 				</div>						 -->
<!-- 			</section> -->
			
		</div>
	</section>
	

		
	</main>
</section>


<div id="overlay_t"></div> 
<div id="popup_layer">
	<section class="panel text-small">
		<div class="row-fluid m-t-small">
        	<div class="span12">
        		<header class="panel-heading">
					<div class="span2">Dashboard 설정</div>
				</header>
				
				<div class="row-fluid row-1-phone">
					<div class="span2">
						<div class="text-center h5">기간 설정</div>
					</div>
					<div class="span1">
						<div class="text-center h5">
							<input type="radio" name="priod" id="d" value="D" <c:if test="${setting.prdCd eq 'D'}">checked="checked"</c:if>/>일
						</div>
					</div>
					<div class="span1">
						<div class="text-center h5">
							<input type="radio" name="priod" id="w" value="W" <c:if test="${setting.prdCd eq 'W'}">checked="checked"</c:if>/>주
						</div>
					</div>
					<div class="span1">
						<div class="text-center h5">
							<input type="radio" name="priod" id="m"  value="M" <c:if test="${setting.prdCd eq 'M'}">checked="checked"</c:if>/>월
						</div>
					</div>
					<div class="span1">
						<div class="text-center h5">
							<input type="radio" name="priod" id="y"  value="Y" <c:if test="${setting.prdCd eq 'Y'}">checked="checked"</c:if>/>년
						</div>
					</div>
				</div>
				
				<div class="row-fluid row-1-phone">
					<div class="line"></div>
				</div>

				<div class="row-fluid row-1-phone">
					<div class="text-left h4">사용할 항목</div><br><br>
				</div>
				
          		<div class="row-fluid row-1-phone" id="sortable">
            		<!-- easypiechart -->
            		<c:choose>
            			<c:when test="${not empty setting.items}">
            				<c:forEach items="${setting.items}" var="item">
            					<c:if test="${item eq 'all'}">
						            <div class="span2" id="all">
							        	<section class="panel">
											<div class="text-center h5"><i class="icon-barcode"></i><strong>설치(전체)</strong></div>
							             </section>
									</div>
								</c:if>
								
								<c:if test="${item eq 'basic'}">
									<div class="span2" id="basic">		              
						              	<section class="panel">
											<div class="text-center h5"><i class="icon-bolt"></i><strong>설치(BASIC)</strong></div>
						                </section>		
									</div>
								</c:if>
								
								<c:if test="${item eq 'cnx'}">	
									<div class="span2" id="cnx">
						            	<section class="panel">		          
									  		<div class="text-center h5"><i class="icon-lock"></i><strong>설치(CNX)</strong></div>
						            	</section>			
									</div>
								</c:if>
								
								<c:if test="${item eq 'record'}">		
									<div class="span2" id="record">
						              <section class="panel">		              	
										<div class="text-center h5"><i class="icon-off"></i><strong>기록건수</strong></div>
						              </section>			
									</div>
								</c:if>
								
								<c:if test="${item eq 'common'}">		
						            <div class="span2" id="common">              
						              <section class="panel">
						              	<div class="text-center h5"><i class="icon-link"></i><string>공동알림건수</string></div>
						              </section>
									</div>
								</c:if>
									
							</c:forEach>
						</c:when>
					</c:choose>
	          	</div>
	          	<!-- 항목 -->
	   			<div class="row-fluid row-1-phone">
					<div class="line"></div>
				</div>
				
	          	<div class="row-fluid row-1-phone">
					<div class="span12 text-center">    
						<a href="#" onclick="fn_apply();return false;" class="btn btn-primary">적용</a>
						<a href="#" onclick="fn_cancel();return false;" class="btn btn-default">취소</a>
					</div>
				</div>
				<!-- button -->
			</div>
		</div>
	</section>
</div>

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->

</body>
</html>

