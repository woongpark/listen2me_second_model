<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta charset="utf-8">
<title>Listen2Me</title>
<meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" type="text/css" href="<c:url value="/app-css.v1.css"/>">
<!--[if lt IE 9]>
<script src="<c:url value="/js/respond.min.js"/>"></script>
<script src="<c:url value="/js/html5.js"/>"></script>
<script src="<c:url value="/js/excanvas.js"/>"></script>
<![endif]-->
<script src="<c:url value="/app-js.v1.js"/>"></script>
<script src="<c:url value="/js/jquery.min.js"/>"></script>
<script src="<c:url value="/js/jquery.form.min.js"/>"></script>
<script src="<c:url value="/js/jquery-ui.min.js"/>"></script>

<!-- <script src="https://www.google.com/jsapi"></script> -->

<script type="text/javascript">

function fn_query(){
	
	var org = $("#org").val();
	
	if (org == '') {
		alert('소속기관을 입력하세요.');
		return;
	}
	
	var param = 'org='+org
	
	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/getorg.do"/>'
		,success : function(data){
			if (data != '') {
				$("#domain").val(data.org);
			}
		}
	});
}

function fn_req() {
	
}

</script>
  
</head>
<body>
  <!-- header -->
  <header id="header" class="navbar navbar-sm bg bg-black">
	  <center>
	  	<h2>
	  		LISTEN@ME PORTAL <br>
	  	</h2>
	  </center>
  </header>
  <!-- / header -->
  <section id="content">
  <div class="row-fluid">
      <div class="span12">
        <section class="panel">
			<header class="panel-heading">책임자 조회</header>
	          	          	
          	<div class="control-group">
          		<div class="controls">
          			<label class="control-label">소속기관</label>
	            	<input type="text" name="org" id="org" placeholder="소속기관 입력" class="span8"/>
	            	<br><button type="button" class="btn btn-small" onclick="fn_query();">조회</button>
	            	<br><br>*주식회사,유한회사,학교법인 등의 문구는 입력하지 않으셔도 됩니다.
	            </div>
			</div>        
			
          	<div class="control-group">
          		<div class="controls">
          			<label class="control-label">이메일</label>
	            	<input type="text" name="id" id="id" placeholder="id 입력" class="span4" />
					@
					<input type="text" name="domain" id="domain" placeholder="domain 입력" class="span4" />
	            </div>
			</div> 

          	<div class="control-group">
          		<div class="controls">
          			<div class="row-fluid">
          				<div class="span4"></div>
          				<div class="span4">
							&nbsp;<button class="btn btn-small btn-primary span3" onclick="fn_req();">요청하기</button>
						</div>
						<div class="span4"></div>
						<div class="span4"></div>
					</div>
	            </div>
			</div> 
                      

        </section>
	</div>
   </div>
  </section>

  <!-- Bootstrap -->
  <!-- app -->
</body>
</html>
