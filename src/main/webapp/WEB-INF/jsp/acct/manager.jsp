<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">

function fn_add(){
	$(location).attr("href" , '<c:url value="/admin/acct/pageadd.do"/>');
}

function goPage(cpage){
	$(location).attr("href" , '<c:url value="/admin/acct/getmanagers.do"/>?cPage='+cpage);
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  <br>
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->

<!-- content -->    
<section id="content">
	<main class="main">	
		<section class="panel text-small">
			<header class="panel-heading">
				<div class="span2">Portal 계정</div>
				<p align="right">
					<a href="javascript:fn_add();" class="btn btn-default btn-small">신규</a>
				</p>
			</header>
			
			<div class="pull-out">

					<table class="table table-striped m-b-none text-small">
						<thead>
							<tr>
								<th style="text-align:center;" width="5%">No</th>
								<th style="text-align:center;" width="10%">구분</th>
		                    	<th style="text-align:center;" width="35%">법인명</th>                    
		                    	<th style="text-align:center;" width="30%">이름</th>
		                    	<th style="text-align:center;" width="20%">Email</th>
		                  </tr>
		                 </thead>
						<tbody>
						<c:choose>
							<c:when test="${not empty managers }">
								<c:forEach items="${managers}" var="manager" varStatus="status">
									<tr onclick="fn_selected('${manager.chgId}');">
										<td style="text-align:center;">${status.count}</td>
										<td style="text-align:center;">
											<c:choose>
												<c:when test="${manager.siteCd eq '01'}">기업</c:when>
												<c:when test="${manager.siteCd eq '02'}">학교</c:when>
												<c:when test="${manager.siteCd eq '03'}">공공</c:when>
											</c:choose>								
										</td>
										<td style="text-align:center;">
											${manager.appNm}
										</td>
										<td style="text-align:center;">${manager.chgNm}</td>
										<td style="text-align:center;">${manager.chgId}</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="5"><p class="text-center">데이터가 없습니다.</p></td>
								</tr>					
							</c:otherwise>
						</c:choose>
							<tr>
								<td colspan="5"></td>
							</tr>							
						</tbody>
					</table>

					
					<div class="clearfix">
						<div class="span12">
			                <div class="span12 text-right text-center-sm" align="right">
								<c:if test="${not empty managers}">
									<div class="pagination pagination-small  m-b-none">    
										<ul>
											<li><a href="goPage(${paging.prevPageNo});">&nbsp;<i class="icon-chevron-left"></i></a></li>
											
									        <c:forEach var="i" begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1">
									            <c:choose>
									                <c:when test="${i eq paging.pageNo}">
									                	<li><a href="javascript:goPage(${i})" class="choice">${i}</a></li>
									                </c:when>
									                <c:otherwise>
									                	<li><a href="javascript:goPage(${i})">${i}</a></li>
									                </c:otherwise>
									            </c:choose>
									        </c:forEach>						
											<li><a href="goPage(${paging.nextPageNo});">&nbsp;<i class="icon-chevron-right"></i></a></li>
										</ul>
									</div>
								</c:if>
			                </div>
			        	</div>
			        </div>
			        
			     <br>
				</div>	

		</section>
		
	</main>
</section>

<!-- content -->

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->

</body>
</html>

