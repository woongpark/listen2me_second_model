<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>
<script type="text/javascript">

$(document).ready(function(){
	fn_init();
});

function fn_init(){
	$("#phoneNo").val('${manager.phoneNo}');
	$("#handHpNo").val('${manager.handHpNo}');
}

function fn_setinfo(){
	var param = $("#frm").serialize();
		
	if ($("#pw").val() != '') {
		if ($("#pw").val() == $("#newpw").val()) {
			alert('새 비밀번호와 현재 비밀번호가 같습니다.');
			return;
		}
		
		if ($("#confirmpw").val() != $("#newpw").val()) {
			alert('새 비밀번호와 확인 비밀번호가 다릅니다.');
			return;
		}
	}
	
	if (!password_check($("#newpw").val())) {
		alert('비밀번호는 숫자, 영어, 특수문자를 결합해서 8자 이상이어야 합니다.');
		return;
	}
	
	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/admin/setinfo.do"/>'
		,success : function(data){
			if( data.code == '0000' ){
				alert("사용자정보를 수정하였습니다.");
				$(location).attr("href" , '<c:url value="/admin/getinfo.do"/>');
			}else{
				if (data.msg != ''){
					alert(data.msg);	
				}
				else {
					alert("시스템 오류가 발생했습니다.");	
				}
			}
		}
	});
}

function password_check( password ) {
    var regex= /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/;    
    return (password != '' && password != 'undefined' && regex.test(password));
}

</script>
</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->
    
<section id="content">
	<main class="main">
<br>
		<section class="panel text-small">	

			<header class="panel-heading">
				<div class="span2">My Info</div>
			</header>
		
					  
			<div class="pull-out">
<!-- 	        	<div class="span12"> -->
<!-- 	          		<div class="row-fluid row-1-phone"> -->
<!-- 	          			<div class="span12"> -->
	          							                
<!-- 					            <div class="pull-out m-t-small"> -->
					            <form name="frm" id="frm">
					              <table class="table table-striped b-t text-small">
				                     <tbody>
				                     	<tr>
				                     		<td class="text-center">ID 관리 </td>
				                     		<td>
				                     			<div class="control-group">
											    	<label class="control-label">${manager.chgId}</label>
<!-- 											        <div class="controls"> -->
<!-- 											        	변경할 ID : <input type="text" name="id" placeholder="test@example.com" class="bg-focus"> -->
<!-- 													</div> -->
											    </div>
				                     		</td>
				                     	</tr>
				                     	<tr>
				                     		<td>비밀번호 변경 </td>
				                     		<td>
				                     			<div class="control-group">
											    	<label class="control-label">비밀번호는 숫자, 영어, 특수문자를 결합해서 8자 이상이어야 합니다.</label>
											        <div class="controls">
											        	<input type="password" name="pw" 	id="pw" placeholder="현재 비밀번호" class="bg-focus"><br>
											        	<input type="password" name="newpw" id="newpw"	placeholder="새 비밀번호" class="bg-focus"><br>
											        	<input type="password" name="confirmpw" id="confirmpw" placeholder="새 비밀번호 확인" class="bg-focus">
													</div>
											    </div>
				                     		</td>
				                     	</tr>
				                     	<tr>
				                     		<td>연락처 </td>
				                     		<td>
				                     			<div class="control-group">
											        <div class="controls">
											        	<input type="text" name="phoneNo" id="phoneNo" class="bg-focus" maxlength="13"> 
													</div>
											    </div>
				                     		</td>
				                     	</tr>
				                     	<tr>
				                     		<td>휴대폰번호 </td>
				                     		<td>
				                     			<div class="control-group">
											        <div class="controls">
											        	<input type="text" name="handHpNo" id="handHpNo" placeholder="연락처" class="bg-focus" maxlength="13">  
 													</div>
											    </div>
				                     		</td>
				                     	</tr>
				                     	
				                  		<tr>
				                     		<td>알림항목 </td>
				                     		<td>
				                     			<div class="control-group">
											        <div class="controls">
											        	<input type="checkbox" name="qnaYn" id="qnaYn" <c:if test="${manager.qnaYn eq 'Y' }"> ckecked="checked" value="Y"</c:if>/>   Q&A 등록시 이메일 수신
 													</div>
											    </div>
				                     		</td>
				                     	</tr>
				                     	
				                  		<tr>
				                     		<td colspan="2">
							              		<div class="span6" align="right">
								              		<a href="javascript:fn_init();" class="btn btn-default">취소</a>
								              	</div>
								             	<div class="span6">
								              		<a href="javascript:fn_setinfo();" class="btn btn-danger">적용</a>
								              	</div>
				                     		</td>
				                     	</tr>				                  
				                     	
				                     </tbody>				              
					              </table>
					              </form>
					              
<!-- 					             </div> -->
					             

<!-- 	          			</div> -->
<!-- 	          		</div> -->
	          		<!--  -->
	          			          		
<!-- 	          	</div> -->

	         </div>
		</section>
	
	</main>
</section>

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->

</body>
</html>

