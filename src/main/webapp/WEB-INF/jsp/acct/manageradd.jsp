<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">

$(window).on("load", function(){
	<sec:authorize ifAnyGranted="ROLE_SUB_MASTER">
		fn_selected();
	</sec:authorize>
});

function fn_selected(){
	var selectedSite = $('#appId').val();

	if (selectedSite == '') {
		alert('고객사를 선택하세요.');
		return ;
	}
	
	var param = "appId=" + selectedSite;
	
// 	alert(param);
	
	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/admin/acct/getsitemgt.do"/>'
		,success : function(data){
			
			$("#siteMgt").html('');
			
			if( data.managers != '' ){
								
				var html = '';
				for ( var i in data.managers ) {
					html += '<tr>';
						html += '<td class="h5" style="text-align:center;">' + (parseInt(i)+1) + '</td>';
						html += '<td class="h5" style="text-align:center;">' + data.managers[i].chgNm + '</td>';
						html += '<td class="h5" style="text-align:center;">' + data.managers[i].chgId + '</td>';
						html += '<td style="text-align:left;">' ;
							html += '<button type="button" class="btn btn-primary btn-small" onclick="fn_pwinit(\'' + data.managers[i].chgId + '\');">비밀번호 초기화</button>&nbsp;' ;
							html += '<button type="button" class="btn btn-white btn-small" onclick="fn_delete(\'' + data.managers[i].chgId + '\');">삭제</button>' ;
						html += '</td>';
					html += '</tr>';
				}
				html += '<tr><td colspan="4" style="text-align:center;"></td></tr>'
				$("#siteMgt").html(html);
				
			}else {
				
				html += '<tr><td class="h5" colspan="4" style="text-align:center;">데이터가 없습니다.</td></tr>'
				html += '<tr><td colspan="4" style="text-align:center;"></td></tr>'
				
				$("#siteMgt").html(html);
			}
		}
	});
}

function fn_pwinit( chgId ){
	var param 	= 	"chgId=" + chgId;

	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/admin/setinitpw.do"/>'
		,success : function(data){
			if( data.code == '0000' ){
				alert("비밀번호를 초기화하였습니다.");
			}else{
				alert("시스템 오류가 발생했습니다.");
			}
		}
	});
}

function fn_delete( chgId ){
	
	var param 	= 	"chgId=" + chgId;
	
	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/admin/deletemanager.do"/>'
		,success : function(data){
			if( data.code == '0000' ){
				alert("매니저를 삭제하였습니다.");
				fn_selected()
			}else{
				alert("시스템 오류가 발생했습니다.");
			}
		}
	});
	
}

function fn_add(){
	var appId 	= $('#appId').val();
	var chgId	= $('#chgId').val();
	var chgNm	= $('#chgNm').val();
	var phoneNo	= $('#phoneNo').val();
	
	var chgTp 	= $(":input:radio[name=chgTp]:checked").val();
	
	if ( chgNm == '') {
		alert('매니저명을 입력하세요.');
		$("#chgNm").focus();
		return;
	}
	
   	if (!email_check(chgId)) {
		alert('아이디 형식은 이메일로 해주세요.');
		$("#chgId").focus();
		return;
   	}
   	
	if ( phoneNo == '') {
		alert('연락처를 입력하세요.');
		$("#phoneNo").focus();
		return;
	}
	
	var param 	= 	"appId=" + appId;
		param	+= 	"&chgId=" + chgId;
		param	+= 	"&chgNm=" + chgNm;
		param	+= 	"&phoneNo=" + phoneNo;
		param	+= 	"&chgTp=" + chgTp;
		
		$.ajax({
			 type : "POST"
			,data : param
			,url  : '<c:url value="/admin/setmanager.do"/>'
			,success : function(data){
				if( data.code == '0000' ){
					alert("매니저를 추가하였습니다.");
					fn_selected();
				}else{
					alert("시스템 오류가 발생했습니다.");
				}
			}
		});
}

function onlyHp(obj) {
    $(obj).keyup(function(){
         $(this).val($(this).val().replace(/[^0-9-]/g,""));
    }); 
}

function email_check( email ) {
    var regex=/([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;    
    return (email != '' && email != 'undefined' && regex.test(email));
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  <br>
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->

<!-- content -->    
<section id="content">
	<main class="main">	
		<section class="panel text-small">
			<header class="panel-heading">
				<div class="span2">Portal 계정</div>
			</header>
			
			<div class="pull-out">

				<table class="table table-striped b-t text-small">
	                    <tbody>
	                    	<tr>
	                    		<td class="text-center">고객사 명 
	                    		<td>
									<div class="control-group">
										<select	name="appId" id="appId" onchange="fn_selected();">
											<c:choose>
												<c:when test="${not empty customNms}">
													<sec:authorize ifAnyGranted="ROLE_MASTER">
														<option value=''>--고객사명을 선택하세요.--</option>
														<c:forEach items="${customNms}" var="nm">
															<option value="${nm.siteId}">${nm.siteNm }</option>
														</c:forEach>
													</sec:authorize>
													
													<sec:authorize ifAnyGranted="ROLE_SUB_MASTER">
														<c:forEach items="${customNms}" var="nm">
															<c:if test="${nm.siteId eq siteId}">
																<option value="${nm.siteId}" selected="selected">${nm.siteNm }</option>
															</c:if>
														</c:forEach>
													</sec:authorize>
													
												</c:when>
												<c:otherwise>
													<option value=""></option>
												</c:otherwise>
											</c:choose>
										</select>
								    </div>
	                    		</td>
	                    	</tr>
	                    	<tr>
	                    		<td>매니저명 </td>
	                    		<td>
	                    			<div class="control-group">
								        <input type="text" name="chgNm" id="chgNm" class="bg-focus">
							    	</div>
	                    		</td>
	                    	</tr>
	                    	<tr>
	                    		<td>Email </td>
	                    		<td>
	                    			<div class="control-group">
							        	<input type="text" name="chgId" id="chgId" class="bg-focus"> 
									</div>
	                    		</td>
	                    	</tr>
	                    	<tr>
	                    		<td>연락처 </td>
	                    		<td>
	                    			<div class="control-group">					   
							        	<input type="text" name="phoneNo" id="phoneNo" placeholder="연락처" class="bg-focus" maxlength="13" onkeydown="onlyHp(this)">  
							    	</div>
	                    		</td>
	                    	</tr>
	                    	
	                    	<tr>
	                    		<td>사용자타입 </td>
	                    		<td>
	                    			<div class="control-group">					   
							        	<input type="radio" name="chgTp" id="manager" 	value="002">  portal manager
							        	<input type="radio" name="chgTp" id="user" 		value="003" checked="checked">  portal user
							    	</div>
	                    		</td>
	                    	</tr>	     
	                    	
	                 		<tr>
	                    		<td> </td>
	                    		<td>
	                    			<div class="control-group">
										<a href="javascript:fn_add();" class="btn btn-default btn-small">생성</a>
<!-- 										<a href="javascript:fn_setinfo();" class="btn btn-danger btn-small">비밀번호초기화</a> -->
							    	</div>
	                    		</td>
	                    	</tr>
	                    	
	                    </tbody>				              
	              	</table>
					<div class="line line-dashed"></div>
					<table class="table table-striped m-b-none text-small">
						<thead>
							<tr>
								<th class="h5" style="text-align:center;" width="5%">No</th>
								<th class="h5" style="text-align:center;" width="30%">User 이름</th>
		                    	<th class="h5" style="text-align:center;" width="30%">Email</th>                    
		                    	<th style="text-align:center;" >&nbsp;&nbsp;</th>
		                  </tr>
		                 </thead>
						<tbody id="siteMgt">
							<tr>
								<td class="h5" colspan="4" style="text-align:center;">데이터가 없습니다.</td>
							</tr>
							

<!-- 							<tr> -->
<!-- 								<td class="h5" style="text-align:center;">1</td> -->
<!-- 								<td class="h5" style="text-align:center;">tst</td> -->
<!-- 								<td class="h5" style="text-align:center;">tes@naver.com</td> -->
<!-- 								<td class="h5" style="text-align:center;"> -->
<!-- 									<button type="button" class="btn btn-primary btn-small" onclick="fn_pwinit();">비밀번호 초기화</button> -->
<!-- 									<button type="button" class="btn btn-white btn-small" onclick="fn_delete();">삭제</button> -->
<!-- 								</td> -->
<!-- 							</tr>	 -->
														
							<tr>
								<td colspan="4" style="text-align:center;"></td>
							</tr>											
						</tbody>
					</table>
					

		     <br>
			</div>	

		</section>
		
	</main>
</section>

<!-- content -->

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->

</body>
</html>

