<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>


<script type="text/javascript">

$(function() {
    $("#datepicker").datepicker({
        dateFormat:'yymmdd',
        monthNamesShort:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
        dayNamesMin:['일','월','화','수','목','금','토'],
        changeMonth:true, // 월변경가능
        changeYear:true,  // 년변경가능
        showMonthAfterYear:true // 년 뒤에 월표시  
    });
});

$(document).ready(function(){
	<c:if test = "${board.displayEn eq 'Y'}" >
		$("#displayEn").prop("checked", true);
	</c:if>
});

function fn_cancel(){
	$(location).attr("href" , '<c:url value="/admin/getboards.do"/>');
}

function fn_displayen(){
	if ($("#displayEn").is(":checked")) {
		$("#datepicker").val('');
	}
}

function fn_reservedt(){
	if ($("#displayEn").is(":checked")) {
		$("#displayEn").prop("checked" , false);
	}
}

function fn_updateboard(){
	var param = $("#frm").serialize();
	
	if ($("#title").val() == '') {
		alert('제목을 입력하세요.');
		return;
	}
	
	if ($("#content_txt").val() == '') {
		alert('내용을 입력하세요.');
		return;
	}
	
	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/admin/updateboard.do"/>'
		,success : function(data){
			if( data.code == '0000' ){
				alert("공지사항를 수정하였습니다.");
				$(location).attr("href" , '<c:url value="/admin/getboards.do"/>');
			}else{
				alert("시스템 오류가 발생했습니다.");
			}
		}
	});
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->
    
<section id="content">
	<main class="main">
	<div class="clearfix">
        <h4><i class="icon-edit"></i>공지사항 수정</h4>
    </div>
    
	<div class="row-fluid">
        <div class="span12">
          <section class="panel">
		  <form class="form-horizontal" name="frm" id="frm">
		  		<input type="hidden" name="boardId" id="boardId" value="${board.boardId}"/>
              <div class="row-fluid">
			  
                <div class="span12">

                  <div class="control-group">
                    <label class="control-label">제목</label>
                    <div class="controls">
                      <input type="text" name="title" id="title" class="bg-focus" style="width:70%;" value="${board.title}">
					</div>
                  </div>
                                   
                  <div class="control-group">
                    <label class="control-label">내용</label>
                    <div class="controls">
                      <textarea name="content" id="context_txt" rows="10" class="input-xlarge" style="width:70%;">${board.content}</textarea>
                    </div>
                  </div>
                  
                  <div class="control-group">
                    <label class="control-label">개시일</label>
                    <div class="controls">
                     	<input type="text" id="datepicker" name="reserveDt" onchange="fn_reservedt();" readonly="readonly" value="${board.reserveDt}"/>
                     	<div class="checkbox">
	                        <label>
	                          <input type="checkbox" name="displayEn" id="displayEn" value="Y" onclick="fn_displayen();"> 즉시
	                        </label>
                      	</div>
                    </div>
                  </div>
                 </form>
                  <div class="control-group">
                    <div class="controls">                      
                      <button type="button" class="btn btn-white" onclick="fn_cancel();">취소</button>
                      <button type="button" class="btn btn-primary" onclick="fn_updateboard();">게시</button>
                    </div>
                  </div>
                </div>
                
              </div>
          </section>
		</div>
      </div>
      
    </main>
</section>

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->


</body>
</html>

