<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">

function fn_cancel(){
	$(location).attr("href" , '<c:url value="/admin/getreports.do"/>');
}

function fn_setinfo(reportSeq){
	$(location).attr("href" , '<c:url value="/admin/getreport.do"/>?reportSeq='+reportSeq);
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->
    
<!-- content -->    
<section id="content">
	<main class="main">
		<div class="clearfix">
	        <h4><i class="icon-bullhorn"></i>리포트 관리</h4>
	    </div>
	    
	    

		<div class="row-fluid row-1-phone">
	        <div class="span12">
				<!-- .breadcrumb -->
				<ul class="breadcrumb">
					<li><a href="#"><i class="icon-home"></i>리포트 목록</a></li>
					<li class="active"><a href="#"><i class="icon-list-ul"></i>리포트 상세</a></li>
				</ul>
				<!-- / .breadcrumb -->
			</div>
	    </div>	   

	    
	    <section class="panel text-large">
			<div class="row-fluid m-t-small">
	        	<div class="span12">
	        	
	        		<div class="row-fluid">
	          			<div class="span12">
							<div class="clearfix">
					        	<h4><i class="icon-retweet"></i>사건내용</h4>
					   	 	</div>   
     		
					   	 	<input type="hidden" name="reportSeq" id="reportSeq" value="${report.reportSeq}">	
							<table class="table table-striped b-t text-small">
				                     <tbody>
				                     	<tr>
				                     		<td style="text-align:left;width:20%;" >제목</td>
				                     		<td style="text-align:left;">
												${planJson.title}
				                     		</td>
				                     	</tr>
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">언제 있었던 일인가요?</td>
				                     		<td style="text-align:left;">
												${planJson.starttime} - ${planJson.end_time }
				                     		</td>
				                     	</tr>				                     	
				                
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">어떤 일정이었나요?</td>
				                     		<td style="text-align:left;">
												${planJson.meeting }
				                     		</td>
				                     	</tr>		
				                     	
				                  		<tr>
				                     		<td style="text-align:left;width:20%;">어느 곳에서 일어났나요?</td>
				                     		<td style="text-align:left;">
												${planJson.place }
				                     		</td>
				                     	</tr>	   	
				                     	
				                  		<tr>
				                     		<td style="text-align:left;width:20%;">왜 그 곳에 가셨나요?(복수선택)</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     				<c:choose>
				                     					<c:when test="${not empty planJson.survey_03 }">
						                     				<c:forEach items="${planJson.survey_03}" var="survey_03">
						                     					<c:if test="${survey_03 eq '1' }"><c:set var="survey_03_1" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_1 eq 'Y' }">checked="checked"</c:if>> 식사
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '2' }"><c:set var="survey_03_2" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_2 eq 'Y' }">checked="checked"</c:if>> 회식/뒷풀이/술자리
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '3' }"><c:set var="survey_03_3" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_3 eq 'Y' }">checked="checked"</c:if>> 업무/회의
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '4' }"><c:set var="survey_03_4" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_4 eq 'Y' }">checked="checked"</c:if>> 출장		
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '5' }"><c:set var="survey_03_5" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_5 eq 'Y' }">checked="checked"</c:if>> 연수 혹은 교육 중	
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '6' }"><c:set var="survey_03_6" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_6 eq 'Y' }">checked="checked"</c:if>> 워크샵 혹은 MT	
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '7' }"><c:set var="survey_03_7" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_7 eq 'Y' }">checked="checked"</c:if>> 특별한 목적없는 만남	
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '8' }"><c:set var="survey_03_8" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_8 eq 'Y' }">checked="checked"</c:if>> 데이트	
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '9' }"><c:set var="survey_03_9" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_9 eq 'Y' }">checked="checked"</c:if>> 소개팅	
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '10' }"><c:set var="survey_03_10" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_10 eq 'Y' }">checked="checked"</c:if>> 여행 (업무 외)
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '11' }"><c:set var="survey_03_11" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03_11 eq 'Y' }">checked="checked"</c:if>> 협박						
						                     					</c:if>
						                     					<c:if test="${survey_03 eq '12' }"><c:set var="survey_03_12" value="Y"/>
						                     						<input type="checkbox" name="survey_03" disabled="disabled" <c:if test="${survey_03 eq '12' }">checked="checked"</c:if>> 강제로 끌려감							
						                     					</c:if>
						                     				</c:forEach>
						                     			</c:when>
						                     		</c:choose>
		              													
												</div>
												<br>
												<c:if test="${not empty planJson.survey_etc_03 }">
													<textarea rows="5" class="input-xlarge" style="width:80%;" disabled="disabled">${planJson.survey_etc_03}</textarea>	
												</c:if>
				                     		</td>
				                     	</tr>		
				                     	
				                  		<tr>
				                     		<td style="text-align:left;width:20%;">그 일이 있을 때 동행이나 목격자도 함께 있었나요?</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     				<c:if test="${planJson.witness eq 1}">
														<input type="checkbox" name="witness" disabled="disabled" <c:if test="${planJson.witness eq 1 }">checked="checked"</c:if>> 넵
													</c:if>	
													<c:if test="${planJson.witness eq 2 }">
														<input type="checkbox" name="witness" disabled="disabled" <c:if test="${planJson.witness eq 2 }">checked="checked"</c:if>> 아니요
													</c:if>
													<c:if test="${planJson.witness eq 3 }">
														<input type="checkbox" name="witness" disabled="disabled" <c:if test="${planJson.witness eq 3 }">checked="checked"</c:if>> 모르겠음
													</c:if>	
													<c:if test="${planJson.witness eq 4 }">
														<input type="checkbox" name="witness" disabled="disabled" <c:if test="${planJson.witness eq 4 }">checked="checked"</c:if>> 말하고 싶지 않음
													</c:if>		
												</div>													
				                     		</td>
				                     	</tr>	
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">그 일이 있을 때 동행이나 목격자도 함께 있었나요?</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     				<c:if test="${planJson.otherVictiom eq 1 }">
														<input type="checkbox" name="otherVictiom" disabled="disabled" <c:if test="${planJson.otherVictiom eq 1 }">checked="checked"</c:if>> 넵
													</c:if>	
													<c:if test="${planJson.otherVictiom eq 2 }">
														<input type="checkbox" name="otherVictiom" disabled="disabled" <c:if test="${planJson.otherVictiom eq 2 }">checked="checked"</c:if>> 아니요
													</c:if>
													<c:if test="${planJson.otherVictiom eq 3 }">
														<input type="checkbox" name="otherVictiom" disabled="disabled" <c:if test="${planJson.otherVictiom eq 3 }">checked="checked"</c:if>> 모르겠음
													</c:if>	
													<c:if test="${planJson.otherVictiom eq 4 }">
														<input type="checkbox" name="otherVictiom" disabled="disabled" <c:if test="${planJson.otherVictiom eq 4 }">checked="checked"</c:if>> 말하고 싶지 않음
													</c:if>		
												</div>													
				                     		</td>
				                     	</tr>	
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">무슨 일이 있었나요?(복수선택)</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     			<c:choose>
				                     					<c:when test="${not empty planJson.survey_05 }">
						                     				<c:forEach items="${planJson.survey_05}" var="survey_05">
						                     					<c:if test="${survey_05 eq '1' }"><c:set var="survey_05_1" value="Y"/>
						                     						<input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_1 eq 'Y' }">checked="checked"</c:if>> 나를 성적으로 비하하는 대화를 타인끼리 나눔
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '2' }"><c:set var="survey_05_2" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_2 eq 'Y' }">checked="checked"</c:if>> 성적 의도가 느껴지는 말이나 지시
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '3' }"><c:set var="survey_05_3" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_3 eq 'Y' }">checked="checked"</c:if>> 성적 의도가 느껴지는 행동(제스쳐, 시선)
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '4' }"><c:set var="survey_05_4" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_4 eq 'Y' }">checked="checked"</c:if>> 성적 의도가 느껴지는 메시지(스마트폰, 메모 등)
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '5' }"><c:set var="survey_05_5" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_5 eq 'Y' }">checked="checked"</c:if>> 싫다고 했는데도, 계속 만나자고 괴롭힘 (스토킹)
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '6' }"><c:set var="survey_05_6" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_6 eq 'Y' }">checked="checked"</c:if>> 강제로 술을 따르거나 자기 옆에 앉으라고 함
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '7' }"><c:set var="survey_05_7" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_7 eq 'Y' }">checked="checked"</c:if>> 먹고 싶지 않은 음식, 술, 약 등을 먹였음
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '8' }"><c:set var="survey_05_8" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_8 eq 'Y' }">checked="checked"</c:if>> 카메라나 휴대폰을 이용해 사진, 영상을 촬영함
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '9' }"><c:set var="survey_05_9" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_9 eq 'Y' }">checked="checked"</c:if>> 음란물을 내게 보여줬거나 보여주려 했음
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '10' }"><c:set var="survey_05_10" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_10 eq 'Y' }">checked="checked"</c:if>> 일부러 성기를 노출
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '11' }"><c:set var="survey_05_11" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_11 eq 'Y' }">checked="checked"</c:if>> 사적 만남, (성)관계를 가지자고 강요
						                     					</c:if>
						                     					<c:if test="${survey_05 eq '12' }"><c:set var="survey_05_12" value="Y"/>
						                     						<br/><input type="checkbox" name="survey_05" disabled="disabled" <c:if test="${survey_05_12 eq 'Y' }">checked="checked"</c:if>> 원치않는 신체적 접촉을 요구했거나, 강제 접촉을 당함 (성추행, 성폭행)
						                     					</c:if>
						                     				</c:forEach>
						                     			</c:when>
						                     		</c:choose>
																												
												</div>													
				                     		</td>
				                     	</tr>	
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">어떤 피해를 입으셨나요?(복수선택)</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     			<c:choose>
				                     					<c:when test="${not empty planJson.survey_06 }">
						                     				<c:forEach items="${planJson.survey_06}" var="survey_06">
						                     					<c:if test="${survey_06 eq '1' }"><c:set var="survey_06_1" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_1 eq 'Y' }">checked="checked"</c:if>> 자기 신체를 만져달라고 요구
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '2' }"><c:set var="survey_06_2" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_2 eq 'Y' }">checked="checked"</c:if>> 제 몸을 강제로 만짐
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '3' }"><c:set var="survey_06_3" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_3 eq 'Y' }">checked="checked"</c:if>> 도구를 이용해 괴롭힘	
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '4' }"><c:set var="survey_06_4" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_4 eq 'Y' }">checked="checked"</c:if>> 폭행을 당함
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '5' }"><c:set var="survey_06_5" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_5 eq 'Y' }">checked="checked"</c:if>> 강제로 옷을 벗기려 했음
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '6' }"><c:set var="survey_06_6" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_6 eq 'Y' }">checked="checked"</c:if>> 강제로 옷을 벗김
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '7' }"><c:set var="survey_06_7" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_7 eq 'Y' }">checked="checked"</c:if>> 저를 죽이려고 했음
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '8' }"><c:set var="survey_06_8" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_8 eq 'Y' }">checked="checked"</c:if>> 자기 성기를 내 몸(성기 제외)에 접촉하거나 넣으려했음 (미수)
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '9' }"><c:set var="survey_06_9" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_9 eq 'Y' }">checked="checked"</c:if>> 자기 성기를 내 몸(성기 제외)에 접촉하거나 넣음 (유사강간)
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '10' }"><c:set var="survey_06_10" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_10 eq 'Y' }">checked="checked"</c:if>> 나와 강제로 성관계를 맺으려고 시도함(미수)
						                     					</c:if>
						                     					<c:if test="${survey_06 eq '11' }"><c:set var="survey_06_11" value="Y"/>
						                     						<br/><input type="checkbox" disabled="disabled" <c:if test="${survey_06_11 eq 'Y' }">checked="checked"</c:if>> 강제로 성관계를 당함 (강간)
						                     					</c:if>
						                     				</c:forEach>
						                     			</c:when>
						                     		</c:choose>
																																
												</div>													
				                     		</td>
				                     	</tr>		
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">구체적인 사건내용을 알려주세요.</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     				<c:if test="${not empty planJson.detailDamage}">
														<textarea rows="5" class="input-xlarge" style="width:80%;" disabled="disabled">${planJson.detailDamage}</textarea>
													</c:if>
												</div>													
				                     		</td>
				                     	</tr>     
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">그 일이 벌어질 때 어떻게 하셨나요(복수선택)</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     			<c:choose>
				                     					<c:when test="${not empty planJson.survey_08 }">
						                     				<c:forEach items="${planJson.survey_08}" var="survey_08">
						                     					<c:if test="${survey_08 eq '1' }"><c:set var="survey_08_1" value="Y"/>
						                     						<input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_1 eq 'Y' }">checked="checked"</c:if>> 거부 의사를 말로 표시했어요
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '2' }"><c:set var="survey_08_2" value="Y"/>
						                     						<input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_2 eq 'Y' }">checked="checked"</c:if>> 거부 의사를 행동으로 표시했어요
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '3' }"><c:set var="survey_08_3" value="Y"/>
						                     						<br><input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_3 eq 'Y' }">checked="checked"</c:if>> 스스로 자리를 피했어요
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '4' }"><c:set var="survey_08_4" value="Y"/>
						                     						<br><input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_4 eq 'Y' }">checked="checked"</c:if>> 주변인 도움으로 자리를 피했어요
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '5' }"><c:set var="survey_08_5" value="Y"/>
						                     						<br><input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_5 eq 'Y' }">checked="checked"</c:if>> 자고 있거나 의식이 없는 상태였어요
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '6' }"><c:set var="survey_08_6" value="Y"/>
						                     						<br><input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_6 eq 'Y' }">checked="checked"</c:if>> 술이나 약 때문에 아무것도 할 수 없었어요
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '7' }"><c:set var="survey_08_7" value="Y"/>
						                     						<br><input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_7 eq 'Y' }">checked="checked"</c:if>> 여러명이 있는 분위기를 깰까봐 그냥 참았어요
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '8' }"><c:set var="survey_08_8" value="Y"/>
						                     						<br><input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_8 eq 'Y' }">checked="checked"</c:if>> 당사자만 있고 무서워서 그냥 참았어
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '9' }"><c:set var="survey_08_9" value="Y"/>
						                     						<br><input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_9 eq 'Y' }">checked="checked"</c:if>> 저항했지만 결국 막지 못했어요
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '10' }"><c:set var="survey_08_10" value="Y"/>
						                     						<br><input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_10 eq 'Y' }">checked="checked"</c:if>> 확실하지 않음
						                     					</c:if>
						                     					<c:if test="${survey_08 eq '11' }"><c:set var="survey_08_11" value="Y"/>
						                     						<br><input type="checkbox" name="survey_08" disabled="disabled" <c:if test="${survey_08_11 eq 'Y' }">checked="checked"</c:if>> 말하고 싶지 않아요
						                     					</c:if>
						                     				</c:forEach>
						                     			</c:when>
						                     		</c:choose>

													<br><br>
													<div class="controls">
														<c:if test="${not empty planJson.survey_etc_08}">
															<textarea rows="5" class="input-xlarge" style="width:80%;" disabled="disabled">${planJson.survey_etc_08}</textarea>
														</c:if>
													</div>
												</div>													
				                     		</td>
				                     	</tr>     
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">직후 그 사람은 어떻게 했나요(복수선택)</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     			<c:choose>
				                     					<c:when test="${not empty planJson.survey_09 }">
						                     				<c:forEach items="${planJson.survey_09}" var="survey_09">
						                     					<c:if test="${survey_09 eq '1' }"><c:set var="survey_09_1" value="Y"/>
						                     						<input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_1 eq 'Y' }">checked="checked"</c:if>> 그냥 가 버림
						                     					</c:if>
						                     					<c:if test="${survey_09 eq '2' }"><c:set var="survey_09_2" value="Y"/>
						                     						<br><input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_2 eq 'Y' }">checked="checked"</c:if>> 별 일 없었던 듯 평소처럼 행동함
						                     					</c:if>
						                     					<c:if test="${survey_09 eq '3' }"><c:set var="survey_09_3" value="Y"/>
						                     						<br><input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_3 eq 'Y' }">checked="checked"</c:if>> 사과했음
						                     					</c:if>
						                     					<c:if test="${survey_09 eq '4' }"><c:set var="survey_09_4" value="Y"/>
						                     						<br><input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_4 eq 'Y' }">checked="checked"</c:if>> 연인 혹은 파트너로 지내자고 함
						                     					</c:if>
						                     					<c:if test="${survey_09 eq '5' }"><c:set var="survey_09_5" value="Y"/>
						                     						<br><input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_5 eq 'Y' }">checked="checked"</c:if>> 지금처럼 지내면 내 이익을 보장해 준다고 함
						                     					</c:if>
						                     					<c:if test="${survey_09 eq '6' }"><c:set var="survey_09_6" value="Y"/>
						                     						<br><input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_6 eq 'Y' }">checked="checked"</c:if>> 관계를 거부하면 내게 불이익을 준다고 함
						                     					</c:if>
						                     					<c:if test="${survey_09 eq '7' }"><c:set var="survey_09_7" value="Y"/>
						                     						<br><input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_7 eq 'Y' }">checked="checked"</c:if>> 성적 모욕감을 주는 말이나 행동을 지속함
						                     					</c:if>
						                     					<c:if test="${survey_09 eq '8' }"><c:set var="survey_09_8" value="Y"/>
						                     						<br><input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_8 eq 'Y' }">checked="checked"</c:if>> 이 일을 알리지 말라고 함
						                     					</c:if>
						                     					<c:if test="${survey_09 eq '9' }"><c:set var="survey_09_9" value="Y"/>
						                     						<br><input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_9 eq 'Y' }">checked="checked"</c:if>> 자기 말을 안 들으면 이번 일을 알리겠다고 함
						                     					</c:if>
						                     					<c:if test="${survey_09 eq '10' }"><c:set var="survey_09_10" value="Y"/>
						                     						<br><input type="checkbox" name="survey_09" disabled="disabled" <c:if test="${survey_09_10 eq 'Y' }">checked="checked"</c:if>> 찍은 사진이나 영상을 공개하겠다고 협박
						                     					</c:if>
						                     				</c:forEach>
						                     			</c:when>
						                     		</c:choose>
																																				
													<br><br>
													<div class="controls">
														<c:if test="${not empty planJson.survey_etc_09}">
															<textarea rows="5" class="input-xlarge" style="width:80%;" disabled="disabled">${planJson.survey_etc_09}</textarea>
														</c:if>
													</div>
												</div>													
				                     		</td>
				                     	</tr>   
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">상황이 끝나고 어떻게 하셨나요(복수선택)</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     			<c:choose>
				                     					<c:when test="${not empty planJson.survey_10 }">
						                     				<c:forEach items="${planJson.survey_10}" var="survey_10">
						                     					<c:if test="${survey_10 eq '1' }"><c:set var="survey_10_1" value="Y"/>
						                     						<input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_1 eq 'Y' }">checked="checked"</c:if>> 지금까지 아무 것도 하지 못했어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '2' }"><c:set var="survey_10_2" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_2 eq 'Y' }">checked="checked"</c:if>> 아무렇지 않은 척 평상시처럼 행동했어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '3' }"><c:set var="survey_10_3" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_3 eq 'Y' }">checked="checked"</c:if>> 인터넷 게시판, SNS 상에 어떻게 해야 되는지 도움을 구했어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '4' }"><c:set var="survey_10_4" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_4 eq 'Y' }">checked="checked"</c:if>> 가족, 친지들에게 이야기했어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '5' }"><c:set var="survey_10_5" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_5 eq 'Y' }">checked="checked"</c:if>> 선배, 상사들에게 이야기했어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '6' }"><c:set var="survey_10_6" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_6 eq 'Y' }">checked="checked"</c:if>> 증거가 될 만한 것을 수집했어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '7' }"><c:set var="survey_10_7" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_7 eq 'Y' }">checked="checked"</c:if>> 병원에서 채증, 진단을 받았어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '8' }"><c:set var="survey_10_8" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_8 eq 'Y' }">checked="checked"</c:if>> 피해자 지원기관에서 상담을 받았어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '9' }"><c:set var="survey_10_9" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_9 eq 'Y' }">checked="checked"</c:if>> 병원치료(산부인과, 외과, 정신과 등)를 받았어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '10' }"><c:set var="survey_10_10" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_10 eq 'Y' }">checked="checked"</c:if>> 출근이나 등교를 포함한 외부 외출을 안 했어요
						                     					</c:if>
						                     					<c:if test="${survey_10 eq '11' }"><c:set var="survey_10_11" value="Y"/>
						                     						<br><input type="checkbox" name="survey_10" disabled="disabled" <c:if test="${survey_10_11 eq 'Y' }">checked="checked"</c:if>> 가해자를 만나 사과를 요구했어요
						                     					</c:if>
						                     				</c:forEach>
						                     			</c:when>
						                     		</c:choose>
												
													
													<br><br>
													<div class="controls">
														<c:if test="${not empty planJson.survey_etc_10}">
															<textarea rows="5" class="input-xlarge" style="width:80%;" disabled="disabled">${planJson.survey_etc_10}</textarea>
														</c:if>
													</div>
												</div>													
				                     		</td>
				                     	</tr>  	
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">피해사실을 입증할 만한 증거가 있나요(복수선택)</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     			<c:choose>
				                     					<c:when test="${not empty planJson.files }">
						                     				<c:forEach items="${planJson.files}" var="file">
						                     					<c:if test="${file eq '1' }"><c:set var="file_1" value="Y"/>
						                     						<input type="checkbox" name="files" disabled="disabled" <c:if test="${file_1 eq 'Y' }">checked="checked"</c:if>> 입고 있었던 옷
						                     					</c:if>
						                     					<c:if test="${file eq '2' }"><c:set var="file_2" value="Y"/>
						                     						<br><input type="checkbox" name="files" disabled="disabled" <c:if test="${file_2 eq 'Y' }">checked="checked"</c:if>> 당시 침구류
						                     					</c:if>
						                     					<c:if test="${file eq '3' }"><c:set var="file_3" value="Y"/>
						                     						<br><input type="checkbox" name="files" disabled="disabled" <c:if test="${file_3 eq 'Y' }">checked="checked"</c:if>> 사진
						                     					</c:if>
						                     					<c:if test="${file eq '4' }"><c:set var="file_4" value="Y"/>
						                     						<br><input type="checkbox" name="files" disabled="disabled" <c:if test="${file_4 eq 'Y' }">checked="checked"</c:if>> 음성녹음
						                     					</c:if>
						                     					<c:if test="${file eq '5' }"><c:set var="file_5" value="Y"/>
						                     						<br><input type="checkbox" name="files" disabled="disabled" <c:if test="${file_5 eq 'Y' }">checked="checked"</c:if>> 영상
						                     					</c:if>
						                     					<c:if test="${file eq '6' }"><c:set var="file_6" value="Y"/>
						                     						<br><input type="checkbox" name="files" disabled="disabled" <c:if test="${file_6 eq 'Y' }">checked="checked"</c:if>> 문서, 메모
						                     					</c:if>
						                     					<c:if test="${file eq '7' }"><c:set var="file_7" value="Y"/>
						                     						<br><input type="checkbox" name="files" disabled="disabled" <c:if test="${file_7 eq 'Y' }">checked="checked"</c:if>> 메신저 캡쳐화면
						                     					</c:if>
						                     					<c:if test="${file eq '8' }"><c:set var="file_8" value="Y"/>
						                     						<br><input type="checkbox" name="files" disabled="disabled" <c:if test="${file_8 eq 'Y' }">checked="checked"</c:if>> SMS, 메시지
						                     					</c:if>
						                     					<c:if test="${file eq '9' }"><c:set var="file_9" value="Y"/>
						                     						<br><input type="checkbox" name="files" disabled="disabled" <c:if test="${file_9 eq 'Y' }">checked="checked"</c:if>> 병원채취 DNA
						                     					</c:if>
						                     					<c:if test="${file eq '10' }"><c:set var="file_10" value="Y"/>
						                     						<br><input type="checkbox" name="files" disabled="disabled" <c:if test="${file_10 eq 'Y' }">checked="checked"</c:if>> 없음		
						                     					</c:if>
						                     				</c:forEach>
						                     			</c:when>
						                     		</c:choose>
																													
													<br><br>
													<div class="controls">
														<c:if test="${not empty planJson.files_etc}">
															<textarea rows="5" class="input-xlarge" style="width:80%;" disabled="disabled">${planJson.files_etc}</textarea>
														</c:if>
													</div>
												</div>													
				                     		</td>
				                     	</tr>  		                     		                     	
				                     	<!-- attacker start -->		
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">가해자 정보</td>
				                     		<td style="text-align:left;">
				                     			<c:choose>				                     			
				    								<c:when test="${not empty planJson.attackerInfo}">
				    									<c:forEach items="${planJson.attackerInfo}" var="attacker">
								                     		<table class="table table-striped b-t text-small">
								                     			<tbody>
								                     				<tr>
								                     					<td style="text-align:left;width:20%;">상대방은 OOO 소속인가요</td>
								                     					<td style="text-align:left;">
								                     						<div class="controls">
								                     							<c:if test="${attacker.is_in_your_department eq '1' }">
																					<input type="checkbox" name="is_in_your_department" disabled="disabled" <c:if test="${attacker.is_in_your_department eq '1' }">checked="checked"</c:if>> 넵
																				</c:if>
																				<c:if test="${attacker.is_in_your_department eq '2' }">
																					<input type="checkbox" name="is_in_your_department" disabled="disabled" <c:if test="${attacker.is_in_your_department eq '2' }">checked="checked"</c:if>> 아니요
																				</c:if>
																				<c:if test="${attacker.is_in_your_department eq '3' }">
																					<input type="checkbox" name="is_in_your_department" disabled="disabled" <c:if test="${attacker.is_in_your_department eq '3' }">checked="checked"</c:if>> 정확히 모름
																				</c:if>
																			</div>
								                     					</td>
								                     				</tr>
								                     				<tr>
								                     					<td style="text-align:left;width:20%;">상대방과의 관계</td>
								                     					<td style="text-align:left;">
								                     						<div class="controls">
								                     							<c:if test="${attacker.relation eq '1' }">
																					<input type="checkbox" name="relation" disabled="disabled" <c:if test="${attacker.relation eq '1' }">checked="checked"</c:if>> 직장 상사/선배
																				</c:if>
																				<c:if test="${attacker.relation eq '2' }">
																					<input type="checkbox" name="relation" disabled="disabled" <c:if test="${attacker.relation eq '2' }">checked="checked"</c:if>> 직장 동료
																				</c:if>
																				<c:if test="${attacker.relation eq '3' }">
																					<input type="checkbox" name="relation" disabled="disabled" <c:if test="${attacker.relation eq '3' }">checked="checked"</c:if>> 후배
																				</c:if>
																				<c:if test="${attacker.relation eq '4' }">
																					<input type="checkbox" name="relation" disabled="disabled" <c:if test="${attacker.relation eq '4' }">checked="checked"</c:if>> 친구
																				</c:if>
																				<c:if test="${attacker.relation eq '5' }">
																					<input type="checkbox" name="relation" disabled="disabled" <c:if test="${attacker.relation eq '5' }">checked="checked"</c:if>> 교수/선생
																				</c:if>
																				<c:if test="${attacker.relation eq '6' }">
																					<input type="checkbox" name="relation" disabled="disabled" <c:if test="${attacker.relation eq '6' }">checked="checked"</c:if>> 연인
																				</c:if>
																				<c:if test="${attacker.relation eq '7' }">
																					<input type="checkbox" name="relation" disabled="disabled" <c:if test="${attacker.relation eq '7' }">checked="checked"</c:if>> 친인척
																				</c:if>
																				<c:if test="${attacker.relation eq '8' }">
																					<input type="checkbox" name="relation" disabled="disabled" <c:if test="${attacker.relation eq '8' }">checked="checked"</c:if>> 기타
																				</c:if>
																			</div>
								                     					</td>
								                     				</tr>
								                     				<tr>
								                     					<td style="text-align:left;width:20%;">이름</td>
								                     					<td style="text-align:left;">
								                     						<div class="controls">
																				${attacker.name}
																			</div>
								                     					</td>
								                     				</tr>
								                     				<tr>
								                     					<td style="text-align:left;width:20%;">성별</td>
								                     					<td style="text-align:left;">
								                     						<div class="controls">		
								                     							<c:if test="${attacker.sex eq '1' }">															
																					<input type="checkbox" name="sex" disabled="disabled" <c:if test="${attacker.sex eq '1' }">checked="checked"</c:if>> 남
																				</c:if>
																				<c:if test="${attacker.sex eq '2' }">
																					<input type="checkbox" name="sex" disabled="disabled" <c:if test="${attacker.sex eq '2' }">checked="checked"</c:if>> 여
																				</c:if>
																				<c:if test="${attacker.sex eq '3' }">
																					<input type="checkbox" name="sex" disabled="disabled" <c:if test="${attacker.sex eq '3' }">checked="checked"</c:if>> 모르겠음
																				</c:if>
																			</div>
								                     					</td>
								                     				</tr>
								                     				<tr>
								                     					<td style="text-align:left;width:20%;">부서</td>
								                     					<td style="text-align:left;">
								                     						<div class="controls">																	
																				${attacker.department}
																			</div>
								                     					</td>
								                     				</tr>
								                     				<tr>
								                     					<td style="text-align:left;width:20%;">이메일</td>
								                     					<td style="text-align:left;">
								                     						<div class="controls">																	
																				${attacker.email}
																			</div>
								                     					</td>
								                     				</tr>
								                     				<tr>
								                     					<td style="text-align:left;width:20%;">연락처</td>
								                     					<td style="text-align:left;">
								                     						<div class="controls">																	
																				${attacker.address}
																			</div>
								                     					</td>
								                     				</tr>
								                     				<tr>
								                     					<td style="text-align:left;width:20%;">참고사항</td>
								                     					<td style="text-align:left;">
								                     						<div class="controls">																	
																				${attacker.etc}
																			</div>
								                     					</td>
								                     				</tr>
								                     				<tr>
								                     					<td style="text-align:left;width:20%;">전혀모름</td>
								                     					<td style="text-align:left;">
								                     						<div class="controls">																	
																				<input type="checkbox" name="not" disabled="disabled" <c:if test="${attacker.not_known eq '1' }">checked="checked"</c:if>> 전혀모름
																			</div>
								                     					</td>
								                     				</tr>
								                     			</tbody>
								                     		</table>
								                     		<br>
								                     	</c:forEach>
								                     </c:when>
						                     	</c:choose>
				                     		</td>				                     	
				                     	</tr>
				                     					                     	
				                     	<!-- attacker end -->
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">어떤 해결책을 원하세요 (복수 선택)</td>
				                     		<td style="text-align:left;">
				                     			<div class="controls">
				                     			<c:choose>
				                     					<c:when test="${not empty planJson.survey_13 }">
						                     				<c:forEach items="${planJson.survey_13}" var="survey_13">
						                     					<c:if test="${survey_13 eq '1' }"><c:set var="survey_13_1" value="Y"/>
						                     						<input type="checkbox" name="survey_13" disabled="disabled" <c:if test="${survey_13_1 eq 'Y' }">checked="checked"</c:if>> 상대방의 진심어린 사과
						                     					</c:if>
						                     					<c:if test="${survey_13 eq '2' }"><c:set var="survey_13_2" value="Y"/>
						                     						<br><input type="checkbox" name="survey_13" disabled="disabled" <c:if test="${survey_13_2 eq 'Y' }">checked="checked"</c:if>> 조직 내 가해내용 공개 및 재발방지
						                     					</c:if>
						                     					<c:if test="${survey_13 eq '3' }"><c:set var="survey_13_3" value="Y"/>
						                     						<br><input type="checkbox" name="survey_13" disabled="disabled" <c:if test="${survey_13_3 eq 'Y' }">checked="checked"</c:if>> 상대방과 다른 곳에서 근무/활동
						                     					</c:if>
						                     					<c:if test="${survey_13 eq '4' }"><c:set var="survey_13_4" value="Y"/>
						                     						<br><input type="checkbox" name="survey_13" disabled="disabled" <c:if test="${survey_13_4 eq 'Y' }">checked="checked"</c:if>> 상대방에 대한 조직 내 징계
						                     					</c:if>
						                     					<c:if test="${survey_13 eq '5' }"><c:set var="survey_13_5" value="Y"/>
						                     						<br><input type="checkbox" name="survey_13" disabled="disabled" <c:if test="${survey_13_5 eq 'Y' }">checked="checked"</c:if>> 조직으로부터 상대방 퇴출 (해고/퇴학)
						                     					</c:if>
						                     					<c:if test="${survey_13 eq '6' }"><c:set var="survey_13_6" value="Y"/>
						                     						<br><input type="checkbox" name="survey_13" disabled="disabled" <c:if test="${survey_13_6 eq 'Y' }">checked="checked"</c:if>> 형사처벌
						                     					</c:if>
						                     					<c:if test="${survey_13 eq '7' }"><c:set var="survey_13_7" value="Y"/>
						                     						<br><input type="checkbox" name="survey_13" disabled="disabled" <c:if test="${survey_13_7 eq 'Y' }">checked="checked"</c:if>> 금전적 보상	
						                     					</c:if>
						                     				</c:forEach>
						                     			</c:when>
						                     		</c:choose>													
																												
													<br><br>
													<div class="controls">
														<c:if test="${not empty planJson.survey_etc_13}">
															<textarea rows="5" class="input-xlarge" style="width:80%;" disabled="disabled">${planJson.survey_etc_13}</textarea>
														</c:if>
													</div>
												</div>													
				                     		</td>
				                     	</tr> 
				                     	
				                     	<tr>
				                     		<td style="text-align:left;width:20%;">더 하시고 싶은 이야기를 들려주세요</td>
				                     		<td style="text-align:left;">				                     			
												<div class="controls">
													<c:if test="${not empty planJson.addStatement}">
														<textarea rows="5" class="input-xlarge" style="width:80%;" disabled="disabled">${planJson.addStatement}</textarea>
													</c:if>
												</div>																								
				                     		</td>
				                     	</tr> 
				                     	
				                     						                     		                     	
				                     	<tr>
				                     		<td colspan="2"></td>
				                     	</tr>
				                     </tbody>
				              </table>	        				
	          				
	          			</div>
	          		</div>
	          		
      				<div class="span12">
	             		<div class="span6" align="right">
	              			<a href="javascript:fn_cancel();" class="btn btn-default">취소</a>
	              		</div>
	             		<div class="span6">
	              		<a href="javascript:fn_setinfo('${param.reportSeq}');" class="btn btn-danger">사건관리</a>
	              		</div>
            		</div>
	        	
	        	</div>
	        </div>
	    </section>
	</main>
</section>

<!-- footer -->
<%@include file="/common/include/footer.jsp"%>
<!-- / footer -->

</body>
</html>

