<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">

function fn_report(){
	$(location).attr("href" , '<c:url value="/admin/getreports.do"/>');
}

function fn_selected(reportSeq){
	$(location).attr("href" , '<c:url value="/admin/getreportdetail.do"/>?reportSeq='+reportSeq);
}

function fn_setting(){
	
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->

<!-- content -->    
<section id="content">
	<main class="main">

	<br>
	<section class="panel text-small">
		<header class="panel-heading">
			<div class="span2">Dashboard</div>
<!-- 			<p align="right"><a href="fn_setting();"><i class="icon-cog"></i></a></p> -->
		</header>
		  <div class="row-fluid m-t-small">
	        <div class="span12">
	          <div class="row-fluid row-1-phone">
	            <!-- easypiechart -->
	            <div class="span2">
	              <section class="panel">
	              	<header class="panel-heading bg-white">
						<div class="text-center h5"><i class="icon-barcode"></i><strong>설치(전체)</strong></div>
	                </header>
	                <div class="pull-in text-center">
	                  <div class="inline">
	                      <span class="h2">${userCnt}</span>
	                  </div>
	                </div>
	              </section>
				</div>
				<div class="span2">
	              <section class="panel">
	              	<header class="panel-heading bg-white">
						<div class="text-center h5"><i class="icon-bolt"></i><strong>설치(BASIC)</strong></div>
	                </header>
	                <div class="pull-in text-center">
	                  <div class="inline">
	                      <span class="h2">${userCnt}</span>
	                  </div>
	                </div>
	              </section>			
				</div>
				<div class="span2">
	              <section class="panel">
	              	<header class="panel-heading bg-white">
						<div class="text-center h5"><i class="icon-lock"></i><strong>설치(CNX)</strong></div>
	                </header>
	                <div class="pull-in text-center">
	                  <div class="inline">
	                      <span class="h2">${reportCnt}</span>
	                  </div>
	                </div>
	              </section>			
				</div>
				<div class="span2">
	              <section class="panel">
	              	<header class="panel-heading bg-white">
						<div class="text-center h5"><i class="icon-off"></i><strong>기록건수</strong></div>
	                </header>
	                <div class="pull-in text-center">
	                  <div class="inline">
	                      <span class="h2">${confirmCnt}</span>
	                  </div>
	                </div>
	              </section>			
				</div>
	            <div class="span2">              
	              <section class="panel">
	              	<header class="panel-heading bg-white">
	              	<div class="text-center h5"><i class="icon-link"></i><string>공동알림건수</string></div>
	                </header>
	                <div class="pull-in text-center">
	                  <div class="inline">
	                      <span class="h2">${serveyCnt}</span>
	                  </div>
	                </div>
	              </section>
				</div>
				
<!-- 				<div class="span2"> -->
<!-- 	              <section class="panel"> -->
<!-- 	              	<header class="panel-heading bg-white"> -->
<!-- 						<div class="text-center h5"><i class="icon-ok"></i><strong>완료건수</strong></div> -->
<!-- 	                </header> -->
<!-- 	                <div class="pull-in text-center"> -->
<!-- 	                  <div class="inline"> -->
<%-- 	                      <span class="h2">${finishCnt}</span> --%>
<!-- 	                  </div> -->
<!-- 	                </div> -->
<!-- 	              </section>			 -->
<!-- 				</div> -->
				
	            <!-- easypiechart end-->
	          </div>
			</div>
		</div>
	</section>

	<br>
	
	<section class="panel text-small">'
		<div class="row-fluid">
		
			<section class="panel">
				<div class="row-fluid">
				
			            <div class="span2">
			              <section class="panel">
			              	<header class="panel-heading bg-white">
								<div class="h5"><strong>고객사별 현황</strong></div>
			                </header>
			                <div class="pull-in text-center">
			                  <div class="inline">
			                      <span class="h5">전체</span><br>
			                      <span class="h5">기업</span><br>
			                      <span class="h5">학교</span><br>
			                      <span class="h5">공공</span>
			                  </div>
			                </div>
			              </section>
						</div>		
						
			            <div class="span10">
			              <section class="panel">
			              	<header class="panel-heading bg-white">
								<div class="h5"><strong>문의사항</strong></div>
			                </header>
			                <div class="pull-in text-center">
								<table class="table table-striped m-b-none text-small">
										<thead>
											<tr>
												<th style="text-align:center;" width="20%">고객사</th>
						                    	<th style="text-align:center;" width="50%">문의제목</th>                    
						                    	<th style="text-align:center;">문의일</th>
						                    	<th style="text-align:center;">처리여부</th>
						                  </tr>
						                 </thead>
										<tbody>
										<c:choose>
											<c:when test="${not empty reports }">
												<c:forEach items="${reports}" var="report" varStatus="status">
													<tr onclick="fn_selected('${report.reportSeq}');">
														<td>
															${report.title}
															<c:if test="${empty report.title}">${report.rgsDt} 사건</c:if>									
														</td>
														<td style="text-align:center;">
															<c:choose>
																<c:when test="${report.reportStatus eq 'R'}">이용자신고</c:when>
																<c:when test="${report.reportStatus eq 'C'}">신고확인</c:when>
																<c:when test="${report.reportStatus eq 'I'}">사건접수/조사중</c:when>
																<c:when test="${report.reportStatus eq 'W'}">처분대기</c:when>
																<c:when test="${report.reportStatus eq 'F'}">완결</c:when>
															</c:choose>									
														</td>
														<td style="text-align:center;">${report.rgsDt}</td>
														<td style="text-align:center;"><fmt:formatNumber value="${report.reportSeq}" pattern="00000"/></td>
													</tr>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<tr>
													<td style="text-align:center;" colspan="4">데이터가 없습니다.</td>
												</tr>					
											</c:otherwise>
										</c:choose>
											<tr>
												<td colspan="4"></td>
											</tr>							
										</tbody>
									</table>
			                </div>
			              </section>
						</div>		
				
				
					
				</div>
				
						
			</section>
		</div>
	</section>
	

		
	</main>
</section>

<!-- content -->

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->

</body>
</html>

