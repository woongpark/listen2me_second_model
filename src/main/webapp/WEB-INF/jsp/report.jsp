<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">

function fn_cancel(){
// 	$("#status").val('');
// 	$("#reason").val('');
	
	$(location).attr("href" , '<c:url value="/admin/getreports.do"/>');
}

function fn_setreport(){
	var param = $("#frm").serialize();
	
	if ($("#status").val() == '') {
		alert('사건처리을 선택하세요.');
		$("#status").focus();
		return;
	}
	
	var id = $("#reportSeq").val();
	
	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/admin/setreportstatus.do"/>'
		,success : function(data){
			if( data.code == '0000' ){
				alert("사건관리를 수정하였습니다.");
				$(location).attr("href" , '<c:url value="/admin/getreport.do"/>?reportSeq='+id);
			}else{
				alert("시스템 오류가 발생했습니다.");
			}
		}
	});
}

function fn_setconfirm(){
	$("#status").val('I');
	$("#reason").val('${report.serveyTxt}');
}

function fn_setresult(){
	$("#status").val('W');
	$("#reason").val('${report.resultTxt}');
}


</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->
    
<!-- content -->    
<section id="content">
	<main class="main">
		<div class="clearfix">
	        <h4><i class="icon-bullhorn"></i>리포트 관리</h4>
	    </div>
	    
		<section class="panel text-small">				  
			<div class="row-fluid m-t-small">
		        <div class="span12">
		        	<div class="row-fluid row-1-phone">
	          			<div class="span12">
							<!-- .breadcrumb -->
							<ul class="breadcrumb">
								<li><a href="#"><i class="icon-home"></i>리포트 목록</a></li>
								<li class="active"><a href="#"><i class="icon-list-ul"></i>리포트 관리</a></li>
							</ul>
							<!-- / .breadcrumb -->
						</div>
					</div>
				</div>
		    </div>	    
	    </section>
	    
	    <section class="panel text-small">
			<div class="row-fluid m-t-small">
	        	<div class="span12">
	        	
	        		<div class="row-fluid">
	          			<div class="span12">
							<div class="clearfix">
					        	<h4><i class="icon-bullhorn"></i>사건관리</h4>
					   	 	</div>   
					   	 	<form name="frm" id="frm">       		
					   	 		<input type="hidden" name="reportSeq" id="reportSeq" value="${report.reportSeq}">	
							<table class="table table-striped b-t text-small">
				                     <tbody>
				                     	<tr>
				                     		<td style="text-align:left;width:10%;" >현재 상황</td>
				                     		<td style="text-align:left;">
<!-- 				                     			<div class="control-group"> -->
<!-- 											    	<label class="control-label"> -->
														<c:choose>
															<c:when test="${report.reportStatus eq 'R'}">이용자신고</c:when>
															<c:when test="${report.reportStatus eq 'C'}">신고확인</c:when>
															<c:when test="${report.reportStatus eq 'I'}">사건접수/조사중</c:when>
															<c:when test="${report.reportStatus eq 'W'}">처분대기</c:when>
															<c:when test="${report.reportStatus eq 'F'}">완결</c:when>
														</c:choose>											    		
<!-- 											    	</label> -->
<!-- 											    </div> -->
				                     		</td>
				                     	</tr>
				                     	<tr>
				                     		<td style="text-align:left;width:10%;">사건처리</td>
				                     		<td style="text-align:left;">
				                     			<select name="status" id="status">
				                     				<option value=""></option>
				                     				<c:if test ="${report.reportStatus eq 'R'}">
				                     					<option value="C">신고확인</option>
				                     				</c:if>
				                     				<option value="I">사건접수/조사중</option>
				                     				<option value="W">처분대기</option>
				                     				<option value="F">완결</option>
				                     			</select>
				                     		</td>
				                     	</tr>
				                     	<tr>
				                     		<td style="text-align:left;width:10%;">처리사유</td>
				                     		<td style="text-align:left;">
							                    <div class="controls">
							                      <textarea name="reason" id="reason" rows="5" class="input-xlarge" style="width:70%;"></textarea>
							                    </div>
				                     		</td>
				                     	</tr>			
				                     	<tr>
				                     		<td colspan="2" style="text-align:center;">                    
						                      <button type="button" class="btn btn-white" onclick="fn_cancel();">취소</button>
						                      <button type="button" class="btn btn-primary" onclick="fn_setreport();">입력</button>			                     		
				                     		</td>
				                     	</tr>				                     		                     	
				                     	<tr>
				                     		<td colspan="2"></td>
				                     	</tr>
				                     </tbody>
				              </table>	          				
	          				</form>
	          			</div>
	          		</div>
	          		
	          		<div class="row-fluid">
	          			<div class="span12">
							<div class="clearfix">
					        	<h4><i class="icon-bullhorn"></i>처리기록</h4>
					   	 	</div>
					   	 	
							<table class="table table-striped b-t text-small">
				                     <tbody>
				                     	<tr>
				                     		<td style="text-align:left;width:10%;" >사건명</td>
				                     		<td style="text-align:left;">
												${report.rgsDt} 사건
				                     		</td>
				                     	</tr>
				                     	<tr>
				                     		<td style="text-align:left;width:10%;">이용자 신고</td>
				                     		<td style="text-align:left;">
												${report.rgsDt}
				                     		</td>
				                     	</tr>
				                     	<tr>
				                     		<td style="text-align:left;width:10%;">신고 확인</td>
				                     		<td style="text-align:left;">
												${report.confirmDt}
				                     		</td>
				                     	</tr>		
				         				<tr>
				                     		<td style="text-align:left;width:10%;">사건접수/조사개시</td>
				                     		<td style="text-align:left;">
												${report.serveyDt}
				                     		</td>
				                     	</tr>	
				         				<tr>
				                     		<td style="text-align:left;width:10%;">조사내용</td>
				                     		<td style="text-align:left;">
							                    <div class="controls">
							                      <textarea rows="5" class="input-xlarge" style="width:70%;" readonly="readonly">${report.serveyTxt}</textarea>
							                      <a href="javascript:fn_setconfirm();"><i class="icon-pencil"></i></a>
							                    </div>
				                     		</td>
				                     	</tr>
				         				<tr>
				                     		<td style="text-align:left;width:10%;">처분대기</td>
				                     		<td style="text-align:left;">
												${report.resultDt}
				                     		</td>
				                     	</tr>  
				                  		<tr>
				                     		<td style="text-align:left;width:10%;">처리완료</td>
				                     		<td style="text-align:left;">
							                    <div class="controls">
							                      <textarea rows="5" class="input-xlarge" style="width:70%;" readonly="readonly">${report.resultTxt}</textarea>
							                      <a href="javascript:fn_setresult();"><i class="icon-pencil"></i></a>
							                    </div>
				                     		</td>
				                     	</tr>          				                     		                     	
				                     	<tr>
				                     		<td colspan="2"></td>
				                     	</tr>
				                     </tbody>
				              </table>	          							   	 	
					   	 	
					   	 </div>
					</div>
	        	
	        	</div>
	        </div>
	    </section>
	</main>
</section>

<!-- footer -->
<%@include file="/common/include/footer.jsp"%>
<!-- / footer -->

</body>
</html>

