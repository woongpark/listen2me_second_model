<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta charset="utf-8">
<title>Listen2Me</title>
<meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" type="text/css" href="<c:url value="/app-css.v1.css"/>">
<!--[if lt IE 9]>
<script src="<c:url value="/js/respond.min.js"/>"></script>
<script src="<c:url value="/js/html5.js"/>"></script>
<script src="<c:url value="/js/excanvas.js"/>"></script>
<![endif]-->
<script src="<c:url value="/app-js.v1.js"/>"></script>
<script src="<c:url value="/js/jquery.min.js"/>"></script>
<script src="<c:url value="/js/jquery.form.min.js"/>"></script>
<script src="<c:url value="/js/jquery-ui.min.js"/>"></script>

<script type="text/javascript">

$(document).ready(function(){
	$('#show1').hide();
	$('#show2').hide();
	
	$('input[type="text"]').keydown(function() {
	    if (event.keyCode == 13) {
	        event.preventDefault();
	    }
	});
	
	$('input[type="password"]').keydown(function() {
	    if (event.keyCode == 13) {
	        event.preventDefault();
	    }
	});
});

function fn_login(){
	
	if ( $("#inputId").val() == '' ) {
		alert('아이디를 입력하세요.');
		$("#inputId").focus();
		return ;
	}
	
	var emailAddr = $("#inputId").val();
  	
   	if (!email_check(emailAddr)) {
		alert('아이디 형식은 이메일로 해주세요.');
		$("#inputId").focus();
		return;
   	}
	
	if ( $("#inputPassword").val() == '' ) {
		alert('패스워드를 입력하세요.');
		$("#inputPassword").focus();
		return;
	}
	
	$("#frm").submit();
}

function email_check( email ) {
    var regex=/([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;    
    return (email != '' && email != 'undefined' && regex.test(email));
}


function fn_show( type ){
	if (type == '1') {
		$('#show1').show();
		$('#show2').hide();
	} else if( type == '2') {
		$('#show1').hide();
		$('#show2').show();
	}else {
		$('#show1').hide();
		$('#show2').hide();
	}
}

function fn_newotp(type){
	if ( type == '1') {
		window.open('https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2 ', '_blank'); 
	} else {
		window.open('https://itunes.apple.com/kr/app/google-authenticator/id388497605?mt=8', '_blank'); 
	}
}

</script>
  
</head>
<body>
  <!-- header -->
  <header id="header" class="navbar navbar-sm bg bg-black">
	  <center>
	  	<h2>
	  		LISTEN@ME PORTAL <br>
	  	</h2>
	  	<br><br>
	  	<h4>
	      	본 페이지는  리슨투미 고객사를 위한 관리 페이지입니다.<br>
	    	일반 이용자는 www.listen2me.or.kr로 이동하시기 바랍니다.
	    </h4>
	  </center>
  </header>

  
  <!-- / header -->
  <section id="content">
	  <div class="row-fluid">
	      	<div class="span12">
		        <section class="panel">
					<div class="row-fluid">
						<div class="control-group">
							<div class="span6">
								<div class="row-fluid">
								<section class="panel">
								
									<label class="control-label">기존 고객사로그인</label>
									<br>
									<form name="frm" id="frm" action='<c:url value="logining.do"/>' method="post" class="padder">
									<div class="row-fluid">
										<div class="span7">
											<div class="control-group">
												<div class="control-group">
													<div class="controls">
														<input type="text" name="inputId" id="inputId" placeholder="이메일 입력" class="span12">
													</div>
												</div> 
												<div class="control-group">
													<div class="controls">
														<input type="password" name="inputPassword" id="inputPassword" placeholder="Password" class="bg-focus span12">
													</div>
												</div>	
											</div>
										</div>
										</form>
										<div class="span4">
											<div class="control-group">
												<div class="controls">
													<button class="btn btn-large btn-primary span12" onclick="fn_login();">로그인</button>
												</div>
											</div>									
										</div>
										<div class="span1">		
											<div class="control-group">
												<div class="controls">
													
												</div>
											</div>				
										</div>
									</div>	
								
								</section>
								</div>					
							</div>
						</div>
						
						<div class="control-group">
							<div class="span1">
							</div>
						</div>
						
						<div class="control-group">
							<div class="span5">
						      <div class="row-fluid">
						        <div class="span12">						        	
									<div class="control-group">
										<div class="controls">
										
											<div class="span12 text-center">
												<a href="signin.html" class="btn btn-white btn-block">리슨투미 도입 문의</a>				      	
											</div>
										
										</div>
									</div>				      	
								</div>
						      </div>
							</div>		
						</div>		
					</div>
					
					<div class="row-fluid">
						<div class="control-group">
							<div class="span12">
						      <div class="row-fluid">
						        <div class="span12">
									*로그인 후 아무 행동없이 30분 경과시 자동으로 로그아웃됩니다.		      	
								</div>
						      </div>
							</div>		
						</div>						
					</div>
					
		            <c:if test="${not empty param.fail}">			            	
			            <div class="row-fluid">
							<div class="control-group">
								<div class="span6">
							      <div class="row-fluid">
							        <div class="span12">
				            			<font color="red">
				            				${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
				            			</font>
				            			<c:remove scope="session" var="SPRING_SECURITY_LAST_EXCEPTION"/>	
				            			<br>
									</div>
							      </div>
								</div>		
							</div>	
						</div>		            	
		            </c:if>
					
					<div class="row-fluid">
						<!-- 안내문구 -->
						<div class="control-group">
							<div class="span3">
						      <div class="row-fluid">
						        <div class="span12 text-center">
									<a href="javascript:fn_show('1');" class="btn btn-white btn-block">ID/비밀번호 분실안내</a>				      	
								</div>
						      </div>
							</div>	
							<div class="span3">
						      <div class="row-fluid">
						        <div class="span12 text-center">
									<a href="javascript:fn_show('2');" class="btn btn-white btn-block">2차 로그인 안내</a>				      	
								</div>
						      </div>
							</div>
<!-- 							<div class="span6"> -->
<!-- 						      <div class="row-fluid"> -->
<!-- 						      </div> -->
<!-- 							</div> -->
						</div>
					</div>
					
					<div class="row-fluid" id="show1">
						<div class="control-group">
							<div class="span6">
						      <div class="row-fluid">
						        <div class="span12">
						        	<br>
									ID 찾기:
									본인이 속한 조직의 이메일로 이용하실 수 있습니다. 정확한 ID는 각 고객사의 리슨
									투미 포탈 책임자에게 문의하시기 바랍니다.<br>
									비밀번호 초기화:
									각 고객사의 책임자는 본인 소속의 이용자 비밀번호를 초기화할 수 있습니다. 본인이
									속한 조직의 책임자에게 문의하시기 바랍니다.<br><br>
									<a href="<c:url value='findcharge.do'/>">본인 소속기관 책임자 조회하기 </a> 	
								</div>
						      </div>
							</div>		
						</div>	
					</div>
  
  					<div class="row-fluid" id="show2">
						<div class="control-group">
							<div class="span6">
						      <div class="row-fluid">
						        <div class="span12">
						        	<br>
										본 페이지는 리슨투미 애플리케이션 이용자들의 민감정보를 다루고 있어, Two-Factor 인증이 필수적으로 요구됩니다. 
										아래 링크에서 구글 OTP를 설치하시기 바랍니다.<br>
										<a href="javascript:fn_newotp('1');">안드로이드폰</a>  | 
										<a href="javascript:fn_newotp('2');">아이폰	</a>
								</div>
						      </div>
							</div>		
						</div>	
					</div>
										      
		        </section>
			</div>
	   </div>
  </section>

</body>
</html>
