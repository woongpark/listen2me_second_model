<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta charset="utf-8">
<title>Listen2Me</title>
<meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" type="text/css" href="<c:url value="/app-css.v1.css"/>">
<!--[if lt IE 9]>
<script src="<c:url value="/js/respond.min.js"/>"></script>
<script src="<c:url value="/js/html5.js"/>"></script>
<script src="<c:url value="/js/excanvas.js"/>"></script>
<![endif]-->
<script src="<c:url value="/app-js.v1.js"/>"></script>
<script src="<c:url value="/js/jquery.min.js"/>"></script>
<script src="<c:url value="/js/jquery.form.min.js"/>"></script>
<script src="<c:url value="/js/jquery-ui.min.js"/>"></script>

<!-- <script src="https://www.google.com/jsapi"></script> -->

<script type="text/javascript">

$(document).ready(function(){
	$('#show1').hide();
	
	$('input[type="text"]').keydown(function() {
	    if (event.keyCode == 13) {
	        event.preventDefault();
	    }
	});
});

function fn_otp(){
	
	if ( $("#pin").val() == '' ) {
		alert('OTP를 입력하세요.');
		$("#pin").focus();
		return;
	}
		
	$("#frm").submit();
}

function email_check( email ) {
    var regex=/([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;    
    return (email != '' && email != 'undefined' && regex.test(email));
}

function fn_show( type ){
	if (type == '1') {
		$('#show1').show();
	} else {
		$('#show1').hide();
	}
}

function onlyNumber(obj) {
    $(obj).keyup(function(){
         $(this).val($(this).val().replace(/[^0-9]/g,""));
    }); 
}

</script>
  
</head>
<body>
  <!-- header -->
  <header id="header" class="navbar navbar-sm bg bg-black">
	  <center>
	  	<h2>
	  		LISTEN@ME PORTAL <br>
	  	</h2>
	  </center>
  </header>
  <!-- / header -->
  <section id="content">
  <div class="row-fluid">
      <div class="span12">
        <section class="panel">

          <form name="frm" id="frm" action='<c:url value="/admin/verifyotp.do"/>' method="post" class="padder">
	          	<label class="control-label">OTP 번호를 입력해 주세요.</label>
	          	
	          	<div class="control-group">
	          		<div class="controls">
		            	<input type="text" name="pin" id="pin" placeholder="OTP 입력" class="span9" onkeydown="onlyNumber(this)" maxlength="6">
		            	<br><button type="button" class="btn btn-small" onclick="fn_otp();">제출</button>
		            </div>
				</div>        
           </form>
           <div class="line line-dashed"></div>
           
           <c:if test="${not empty otpYn}">
				<c:if test="${ otpYn eq 'N'}">
					<div class="control-group">
		          		<div class="controls">
	            			<font color="red">
	            				OTP 번호를 확인 해주세요.
	            			</font>
			            </div>	
					</div>					
				</c:if>
			</c:if>
           
           
			<div class="control-group">
          		<div class="controls">
					<label class="control-label">OTP 사용법</label>
					1. 구글 OTP 앱을 실행합니다.<br>
					2. Listen2ME 계정에 생성된 6자리 비밀번호를 확인 후 본 페이지에 입력합니다.
	            </div>	
			</div>	
			    
      		<c:if test="${not empty encodedKey}">
          	<div class="line line-dashed"></div>
          	
          	<div class="control-group">
          		<div class="controls">
					<label class="control-label">최초 등록/휴대폰 변경 시</label>
					1. 구글OTP 앱을 설치합니다. 설치링크 안드로이드|아이폰<br>
					2. 우측 하단의 [등록용 코드생성]버튼을 눌려서 등록을 진행합니다.<br>
					3. 구글 OTP [listen2me.or.kr] 입력하세요.<br>
					4. 아래 등록하기 버튼을 눌려서 나오는 KEY 코드를 앱에 입력합니다.<br><br>
					<button type="button" class="btn btn-small" onclick="fn_show('1');">등록용코드 생성</button>
					&nbsp;
					&nbsp;
	            </div>	
			</div>	
			
			<div class="control-group" id="show1">
          		<div class="controls">					
						<h3>
						<i class="icon-hand-right"> ${encodedKey} </i>      
						</h3>	
<%-- 					<c:import url="${url}"/> --%>
	            </div>	
			</div>
          	
          </c:if>
        </section>
	</div>
   </div>
  </section>

  <!-- Bootstrap -->
  <!-- app -->
</body>
</html>
