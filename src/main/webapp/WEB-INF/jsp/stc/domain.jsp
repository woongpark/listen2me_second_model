<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">

function fn_add(){
	$(location).attr("href" , '<c:url value="/admin/acct/pageadd.do"/>');
}

function goPage(cpage){
	$(location).attr("href" , '<c:url value="/admin/acct/getmanagers.do"/>?cPage='+cpage);
}

function fn_search(){
	if ($("#srhword").val() == '') {
		alert('검색어를 입력하세요.');
		return;
	}
	
	alert($("#srhword").val() );
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  <br>
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->

<!-- content -->    
<section id="content">
	<main class="main">	
		<section class="panel text-small">
			<header class="panel-heading">
				<div class="span2">Domain 관리</div>
			</header>
			
			<div class="pull-out">
			
					<div class="clearfix">
						<div class="span12 h5">
							<br>
							아래 소속값은 이용자가 직접 입력한 정보입니다.통합하고자 하는 항목들을 선택하세요.
							<br>&nbsp;
			        	</div>
			        </div>	
		        
		        	<div class="clearfix">
						<div class="span6 h4 text-left">
							정렬방식은 각 항목 클릭하세요.
			        	</div>
						<div class="span6">
							<form class="navbar-form pull-right shift" action="" data-toggle="shift:appendTo" data-target=".nav-primary">
							      <i class="icon-search text-muted" onclick="fn_search();return false;"></i>
							      <input type="text" id="srhword" name="srhword" class="input-small" placeholder="Search">
							</form> 
			        	</div>
			        </div>		
								
					<table class="table table-striped m-b-none text-small">
						<thead>
							<tr>
								<th style="text-align:center;" width="5%">No</th>
								<th style="text-align:center;" width="10%">고객</th>
		                    	<th style="text-align:center;" width="35%">소속명</th>                    
		                    	<th style="text-align:center;" width="30%">Email Domain</th>
		                    	<th style="text-align:center;" width="20%">카운트</th>
		                  </tr>
		                 </thead>
						<tbody>
						<c:choose>
							<c:when test="${not empty managers }">
								<c:forEach items="${managers}" var="manager" varStatus="status">
									<tr onclick="fn_selected('${manager.chgId}');">
										<td style="text-align:center;">${status.count}</td>
										<td style="text-align:center;">
											<c:choose>
												<c:when test="${manager.siteCd eq '01'}">기업</c:when>
												<c:when test="${manager.siteCd eq '02'}">학교</c:when>
												<c:when test="${manager.siteCd eq '03'}">공공</c:when>
											</c:choose>								
										</td>
										<td style="text-align:center;">
											${manager.appNm}
										</td>
										<td style="text-align:center;">${manager.chgNm}</td>
										<td style="text-align:center;">${manager.chgId}</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="5"><p class="text-center">데이터가 없습니다.</p></td>
								</tr>					
							</c:otherwise>
						</c:choose>
							<tr>
								<td colspan="5"></td>
							</tr>							
						</tbody>
					</table>

					
					<div class="clearfix">
						<div class="span12">
			                <div class="span12 text-right text-center-sm" align="right">
								<c:if test="${not empty managers}">
									<div class="pagination pagination-small  m-b-none">    
										<ul>
											<li><a href="goPage(${paging.prevPageNo});">&nbsp;<i class="icon-chevron-left"></i></a></li>
											
									        <c:forEach var="i" begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1">
									            <c:choose>
									                <c:when test="${i eq paging.pageNo}">
									                	<li><a href="javascript:goPage(${i})" class="choice">${i}</a></li>
									                </c:when>
									                <c:otherwise>
									                	<li><a href="javascript:goPage(${i})">${i}</a></li>
									                </c:otherwise>
									            </c:choose>
									        </c:forEach>						
											<li><a href="goPage(${paging.nextPageNo});">&nbsp;<i class="icon-chevron-right"></i></a></li>
										</ul>
									</div>
								</c:if>
			                </div>
			        	</div>
			        </div>
			        
			     <br>
				</div>	

		</section>
		
	</main>
</section>

<!-- content -->

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->

</body>
</html>

