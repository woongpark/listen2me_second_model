<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">
	function fn_chkDate() {
		
		var from = $("#startDd").val();
		var to 	 = $("#endDd]").val();
		if (from != '' && to != '') {
			if (from > to) {
				alert('종료일이 시작일보다 작습니다.');
				$("#endDd").val('');
				return false;		
			}	
		}	
		return true;
	}
	
	function fn_search(){
		$("#frm").attr("method","POST");
		$("#frm").attr("action",'<c:url value="/admin/getallstc.do"/>');
		$("#frm").submit();
	}
	
	function fn_excel(){
		$("#frm").attr("method","POST");
		$("#frm").attr("action",'<c:url value="/admin/alldownloadExcel.do"/>');
		$("#frm").submit();
	}
	
</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->


<!-- content -->    
<section id="content">
	<main class="main"><br>
	    <section class="panel text-small">
			
			<header class="panel-heading">
				<div class="span2">전체 앱 설치 건수</div>
			</header>
			
			<div class="row-fluid m-t-small">
				<div class="span12">
					<div class="row-fluid row-1-phone">	  
					
						<div class="span12">
			              <section class="panel">
			              	<header class="panel-heading bg-white">
								<div class="text-center h5"><i class="icon-barcode"></i><strong>설치(전체)</strong></div>
			                </header>
			                <div class="pull-in text-center">
			                  <div class="inline">
			                  
<script src="<c:url value="/js/code/highcharts.js"/>"></script>
<script src="<c:url value="/js/code/modules/exporting.js"/>"></script>
<script src="<c:url value="/js/code/themes/grid-light.js"/>"></script>

							<!-- graph -->
							<div id="container" style="height: 500px;"></div>
							

							<script type="text/javascript">
							
							Highcharts.chart('container', {
							    title: {text: '' },
							    xAxis: {
							        categories: [
<c:choose>
	<c:when test="${not empty all}">
		<c:forEach items="${all}" var="record" varStatus="status">
			'${record.date}'
			<c:if test="${!status.last}">,</c:if>
		</c:forEach>
	</c:when>
</c:choose>
							                     ] 
							    },
							    yAxis: {title: { text: ''}},
							    series: [{
							    	name : '설치(전체)' ,
							        data: [
<c:choose>
<c:when test="${not empty all}">
	<c:forEach items="${all}" var="record" varStatus="status">
		${record.cnt}
		<c:if test="${!status.last}">,</c:if>
	</c:forEach>
</c:when>
</c:choose>							               
							               ]
							    }],
							    exporting:false ,
							    credits: {
							        enabled: false
							    }
							});
							</script>
														
							<!-- graph -->


			                  </div>
			                </div>
			              </section>
						</div>
								
					
					</div>				
				</div>
			</div>
			<!-- graph -->
			
		<section class="panel text-small">
			<div class="row-fluid">
			<form name="frm" id="frm">
				<div class="span1">
					<select	name="priod" id="priod">
						<option value="D" <c:if test="${priod eq 'D'}">selected="selected"</c:if>>일</option>
						<option value="W" <c:if test="${priod eq 'W'}">selected="selected"</c:if>>주</option>
						<option value="M" <c:if test="${priod eq 'M'}">selected="selected"</c:if>>월</option>
						<option value="Y" <c:if test="${priod eq 'Y'}">selected="selected"</c:if>>년</option>
					</select>
				</div>
				<div class="span1"></div>
				<div class="span4">
					<input type="text" id="startDd"  name="startDd" value="${startDd}" class="datepicker" onchange="fn_chkDate();"> ~
					<input type="text" id="endDd" 	name="endDd" 	value="${endDd}" class="datepicker" onchange="fn_chkDate();">
				</div>
			</form>
				<div class="span5 text-right">
					<a href="javascript:fn_search();" class="btn btn-default btn-small">재산출</a>
					<a href="javascript:fn_excel();" class="btn btn-default btn-small">엑셀</a>
				</div>
			</div>
		</section>
			

				<div class="row-fluid m-t-small">

					<table class="table table-striped m-b-none text-small">
						<thead>
							<tr>
								<th style="text-align:center;" width="40%">일자</th>
								<th style="text-align:center;" >DATA</th>
		                  </tr>
		                 </thead>
						<tbody>


<c:choose>
<c:when test="${not empty all}">
	<c:forEach items="${all}" var="record" varStatus="status">
						<tr>
							<td style="text-align:center;">${record.date}</td>
							<td style="text-align:center;">${record.cnt}</td>
						</tr>
	</c:forEach>
</c:when>
</c:choose>						

					</table>

				</div>	

	        
	    </section>	    
	 </main>
</section>    


<!-- footer -->
<%@include file="/common/include/footer.jsp"%>
<!-- / footer -->

</body>
</html>

