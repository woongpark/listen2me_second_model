<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">
function fn_all(){
	$(location).attr("href" , '<c:url value="/admin/getallstc.do"/>');
}

function fn_basic(){
	$(location).attr("href" , '<c:url value="/admin/getbasicstc.do"/>');
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->


<!-- content -->    
<section id="content">
	<main class="main"><br>
	    <section class="panel text-small">
			
			<header class="panel-heading">
				<div class="span2">통계</div>
			</header>
			
			<div class="row-fluid m-t-small">
				<div class="span12">
					<div class="row-fluid row-1-phone">	  
					
						<div class="span6">
			              <section class="panel">
			              	<header class="panel-heading bg-white">
								<div class="text-center h5"><i class="icon-barcode"></i><strong>설치(전체)</strong></div>
			                </header>
			                <div class="pull-in text-center">
			                  <div class="inline">
			                  
<script src="<c:url value="/js/code/highcharts.js"/>"></script>
<script src="<c:url value="/js/code/modules/exporting.js"/>"></script>
<script src="<c:url value="/js/code/themes/grid-light.js"/>"></script>

							<!-- graph -->
							<div id="container" style="height: 300px;"></div>
							

							<script type="text/javascript">
							
							Highcharts.chart('container', {
							    title: {text: '' },
							    xAxis: {
							        categories: [
<c:choose>
	<c:when test="${not empty all}">
		<c:forEach items="${all}" var="record" varStatus="status">
			${record.date}
			<c:if test="${!status.last}">,</c:if>
		</c:forEach>
	</c:when>
</c:choose>
							                     ] 
							    },
							    yAxis: {title: { text: ''}},
							    series: [{
							    	name : '설치(전체)' ,
							        data: [
<c:choose>
<c:when test="${not empty all}">
	<c:forEach items="${all}" var="record" varStatus="status">
		${record.cnt}
		<c:if test="${!status.last}">,</c:if>
	</c:forEach>
</c:when>
</c:choose>							               
							               ]
							    }],
							    exporting:false ,
							    credits: {
							        enabled: false
							    }
							});
							</script>
														
							<!-- graph -->


			                  </div>
			                </div>
			              </section>
						</div>
						
						<div class="span6">
			              <section class="panel">
			              	<header class="panel-heading bg-white">
								<div class="text-center h5"><i class="icon-bolt"></i><strong>설치(BASIC)</strong></div>
			                </header>
			                <div class="pull-in text-center">
			                  <div class="inline">


							<!-- graph -->
							<div id="container1" style="height: 300px;"></div>
							

							<script type="text/javascript">
							
							Highcharts.chart('container1', {
							    title: {text: '' },
							    xAxis: {
							        categories: [
<c:choose>
<c:when test="${not empty basic}">
	<c:forEach items="${basic}" var="record" varStatus="status">
		${record.date}
		<c:if test="${!status.last}">,</c:if>
	</c:forEach>
</c:when>
</c:choose>
							                     ] 
							    },
							    yAxis: {title: { text: ''}},
							    series: [{
							    	name : '설치(BASIC)' ,
							        data: [
<c:choose>
<c:when test="${not empty basic}">
	<c:forEach items="${basic}" var="record" varStatus="status">
		${record.cnt}
		<c:if test="${!status.last}">,</c:if>
	</c:forEach>
</c:when>
</c:choose>	
							               ]
							    }] ,
							    exporting:false ,
							    credits: {
							        enabled: false
							    }
							});
							</script>
														
							<!-- graph -->


			                  </div>
			                </div>
			              </section>			
						</div>					
					
					</div>				
				</div>
			</div>
			<!-- graph -->

				<div class="row-fluid m-t-small">

					<table class="table table-striped m-b-none text-small">
						<thead>
							<tr>
								<th style="text-align:center;" width="10%">No</th>
								<th style="text-align:center;" >통계제목</th>
		                  </tr>
		                 </thead>
						<tbody>

						<tr onclick="fn_selected('1');">
							<td style="text-align:center;">1</td>
							<td style="text-align:center;" onclick="fn_all(); return false;">전체 앱 설치건수</td>
						</tr>
						
						<tr onclick="fn_selected('2');">
							<td style="text-align:center;">2</td>
							<td style="text-align:center;"  onclick="fn_basic(); return false;">Basic 앱 설치 건수</td>
						</tr>		
						
						<tr onclick="fn_selected('3');">
							<td style="text-align:center;">3</td>
							<td style="text-align:center;">Basic 기록 발생 건수</td>
						</tr>				
						
						<tr onclick="fn_selected('4');">
							<td style="text-align:center;">4</td>
							<td style="text-align:center;">Basic 공동알림 발생 건수</td>
						</tr>		
						
						<tr onclick="fn_selected('5');">
							<td style="text-align:center;">5</td>
							<td style="text-align:center;">Basic 사건종류별 기록 건수</td>
						</tr>	
						
						<tr onclick="fn_selected('5');">
							<td style="text-align:center;">5</td>
							<td style="text-align:center;">Basic 가해자별 소속 건수</td>
						</tr>

					</table>

				</div>	

	        
	    </section>	    
	 </main>
</section>    


<!-- footer -->
<%@include file="/common/include/footer.jsp"%>
<!-- / footer -->

</body>
</html>

