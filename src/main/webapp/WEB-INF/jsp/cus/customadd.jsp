<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<%@include file="/common/include/header.jsp"%>

<script type="text/javascript">

$( document ).ready(function() {
	
	$('input[type="text"]').keydown(function() {
	    if (event.keyCode == 13) {
	        event.preventDefault();
	    }
	});
	
    $("#noemail").click( function(){
    	if ($("#noemail").is(":checked") == true ){
    		$("#emailUrl").prop("disabled" , "disabled");
    		$("#emailUrl").val('');
    	} else {
    		$("#emailUrl").prop("disabled" , "");
    	}
    });
});
function fn_save(){
	if ($("#siteNm").val() == '') {
		alert('법인명을 입력하세요.');
		return;
	}
	
	if ($("#noemail").is(":checked") != true ){
		if ($("#emailUrl").val() == '') {
			alert('이메일URL을 입력하세요.');
			return;
		}
	}
	
	if ($("#siteModelCd").val() == '') {
		alert('모델구분을 선택하세요.');
		return;
	}
	
	if ($("#custCd").val() == '') {
		alert('고객코드를 입력하세요.');
		return;
	}
	
	if ($("#createDd").val() == '') {
		alert('이용 개시일을 입력하세요.');
		return;
	}
	
	if ($("#limitDd").val() == '') {
		alert('갱신일을 입력하세요.');
		return;
	}
	
	if ($("#cttpeNm").val() == '') {
		alert('계약담당자명 입력하세요.');
		return;
	}
	
	if ($("#cttpeHp").val() == '') {
		alert('계약담당자 연락처를 입력하세요.');
		return;
	}
	
	if ($("#cttpeEmail").val() == '') {
		alert('계약담당자 이메일를 입력하세요.');
		return;
	}
	
	if ($("#portalNm").val() == '') {
		alert('Poral Manager명 입력하세요.');
		return;
	}
	
	if ($("#portalHp").val() == '') {
		alert('Poral Manager 연락처를 입력하세요.');
		return;
	}
	
	if ($("#portalEmail").val() == '') {
		alert('Poral Manager 이메일를 입력하세요.');
		return;
	}
	
	var param = $("#frm").serialize();
	
	$.ajax({
		 type : "POST"
		,data : param
		,url  : '<c:url value="/admin/cus/setcustom.do"/>'
		,success : function(data){
			if( data.code == '0000' ){
				alert("고객사를 추가하였습니다.");
				$(location).attr("href" , '<c:url value="/admin/cus/customs.do"/>');
			}else{
				alert("시스템 오류가 발생했습니다.");
			}
		}
	});
}

function fn_cancel(){
	$(location).attr("href" , '<c:url value="/admin/cus/customs.do"/>');
}

function onlyNumber(obj) {
    $(obj).keyup(function(){
         $(this).val($(this).val().replace(/[^0-9]/g,""));
    }); 
}

function onlyHp(obj) {
    $(obj).keyup(function(){
         $(this).val($(this).val().replace(/[^0-9-]/g,""));
    }); 
}

</script>

</head>
<body>

<!-- header -->
<%@include file="/common/include/top.jsp"%>
<!-- / header -->
  <br>
<!-- nav -->
<%@include file="/common/include/left.jsp"%>
<!-- / nav -->

<!-- content -->    
<section id="content">
	<main class="main">	
	<section class="panel text-small">
		<header class="panel-heading">
			<div class="span2">고객사</div>
			<p align="right"><a href="#" ><i class="icon-hdd"></i></a></p>
		</header>
		
		<div class="row-fluid">
		
        <div class="span9">      
        <form name="frm" id="frm" method="post" class="padder">
        
			<div class="control-group">
			   <label class="control-label">법인명</label>
			   <div class="controls">
			   		<input type="text" name="siteNm" id="siteNm" class="bg-focus" style="width:40%;">
			   		<select name="siteCd" id="siteCd" style="width:30%;">
			   			<option value="">법인구분</option>
			   			<option value="01">기업</option>
			   			<option value="02">학교</option>
			   			<option value="03">공공</option>
			   		</select>
				</div>
			</div>
                              
             <div class="control-group">
               	<label class="control-label">이메일 URL</label>
               	<div class="controls">
                 	<input type="text" name="emailUrl" id="emailUrl" class="bg-focus" style="width:40%;">
                 	<input type="checkbox" name="noemail" id="noemail" style="width:5%;"/>없음
				</div>
			</div>
			
             <div class="control-group">
               	<label class="control-label">모델구분</label>
               	<div class="controls">
                 	<select name="siteModelCd" id="siteModelCd" style="width:70%;">
			   			<option value="">모델구분</option>
			   			<option value="01">BASIC</option>
			   			<option value="02">ClOUD</option>
			   			<option value="03">ENTERPRISE</option>
			   		</select>
				</div>
			</div>
			
			<div class="control-group">
               	<label class="control-label">고객코드(3자리)</label>
               	<div class="controls">
                 	<input type="text" name="custCd" id="custCd" class="bg-focus" style="width:70%;" maxlength="3" onkeydown="onlyNumber(this)">
				</div>
			</div>
			
			<div class="control-group">
               	<label class="control-label">이용개시일</label>
               	<div class="controls">
                 	<input type="text" name="createDd" id="createDd" class="bg-focus" style="width:70%;" onkeydown="onlyNumber(this)" maxlength="8">
				</div>
			</div>
			
			<div class="control-group">
               	<label class="control-label">갱신일</label>
               	<div class="controls">
                 	<input type="text" name="limitDd" id="limitDd" class="bg-focus" style="width:70%;" onkeydown="onlyNumber(this)" maxlength="8">
				</div>
			</div>
			
			<div class="control-group">
               	<label class="control-label">계약담당자</label>
               	<div class="controls">
                 	<input type="text" name="cttpeNm" 		id="cttpeNm" placeholder="이름" class="bg-focus" style="width:70%;">
                 	<br>
                 	<input type="text" name="cttpeHp" 		id="cttpeHp" placeholder="연락처" class="bg-focus" style="width:35%;" onkeydown="onlyHp(this);">
                 	<input type="text" name="cttpeEmail" 	id="cttpeEmail" placeholder="이메일" class="bg-focus" style="width:34%;">
				</div>
			</div>
			
			<div class="control-group">
               	<label class="control-label">Portal Manager</label>
               	<div class="controls">
                 	<input type="text" name="portalNm" id="portalNm" placeholder="이름" class="bg-focus" style="width:70%;">
                 	<br>
                 	<input type="text" name="portalHp" id="portalHp" placeholder="연락처" class="bg-focus" style="width:35%;" onkeydown="onlyHp(this);">
                 	<input type="text" name="portalEmail" id="portalEmail" placeholder="이메일" class="bg-focus" style="width:34%;">
				</div>
			</div>
			</form>
			<div class="control-group">
               	<div class="controls text-center">
					<button type="button" class="btn btn-primary" onclick="fn_save();">완료</button>
					<button type="button" class="btn btn-white" onclick="fn_cancel();">취소</button>
				</div>
			</div>
		</div>	
		</div>
		
	</section>
	
	</main>
</section>

<!-- content -->

<!-- nav -->
<%@include file="/common/include/footer.jsp"%>
<!-- / nav -->

</body>
</html>

