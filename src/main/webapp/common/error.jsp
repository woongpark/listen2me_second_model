<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Mobile first web app theme | first</title>
<meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" type="text/css" href="<c:url value="/app-css.v1.css"/>">
<!--[if lt IE 9]>
    <script src="<c:url value="js/respond.min.js"/>"></script>
    <script src="<c:url value="js/html5.js"/>"></script>
    <script src="<c:url value="js/excanvas.js"/>"></script>
  <![endif]-->
</head>
<body>
  <!-- header -->
  <header id="header" class="navbar navbar-sm bg bg-black">
  <a href="#" class="btn btn-link pull-right" data-toggle="class:bg-inverse" data-target="html">
  	<i class="icon-lightbulb icon-large text-default"></i></a>
	<a class="navbar-brand" href="<c:url value="/admin/main.do"/>">
		LISTEN@ME PORTAL
	</a>
  </header>
  <!-- header end -->
  <section id="content">
  <div class="row-fluid">
        <div class="span4 offset4">
          <div class="text-center m-b-large">
            <h1 class="h text-white">404</h1>
          </div>
        </div>
    </div>
  </section>
  	<!-- footer -->
  	<footer id="footer">
  	<div class="text-center padder clearfix">
      <p>
        <small>&copy; Listen@Me 2018, Social Venture Technology for the Voice of Truth</small><br><br>
      </p>
    </div>
  </footer><!-- / footer -->
  <!-- Bootstrap -->
  <!-- app -->
  <script src="<c:url value="app-js.v1.js"/>"></script>
</body>
</html>
