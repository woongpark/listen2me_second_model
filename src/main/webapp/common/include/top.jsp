<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<header id="header" class="navbar navbar-sm bg bg-black">
	<ul class="nav navbar-nav navbar-avatar pull-right">
		<li>
			<div class="m-t m-b-small">
				<span class="h5">
					<strong>					
						${sessionScope.login.username}
					</strong>
				</span> 
				| <a href="<c:url value="/logouting.do"/>"><i class="icon-signout"></i> Logout</a>
			</div>
		</li>
    </ul>
	<a class="navbar-brand" href="<c:url value="/admin/main.do"/>">
	  	LISTEN@ME 
	  	<sec:authorize ifAnyGranted="ROLE_MASTER">ADMIN</sec:authorize>
	  	<sec:authorize ifAnyGranted="ROLE_SUB_MASTER">PORTAL</sec:authorize>
	</a>
</header>
