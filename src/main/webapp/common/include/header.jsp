<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@	taglib prefix="fmt" 	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" 		uri="http://java.sun.com/jsp/jstl/functions"%>

<meta charset="utf-8">
<title>Listen2Me</title>
<meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" type="text/css" href="<c:url value="/app-css.v1.css"/>">
<!--[if lt IE 9]>
    <script src="<c:url value="/js/respond.min.js"/>"></script>
    <script src="<c:url value="/js/html5.js"/>"></script>
    <script src="<c:url value="/js/excanvas.js"/>"></script>
<![endif]-->
<script src="<c:url value="/app-js.v1.js"/>"></script>
<script src="<c:url value="/js/jquery.min.js"/>"></script>
<script src="<c:url value="/js/jquery.form.min.js"/>"></script>
<script src="<c:url value="/js/jquery-ui.min.js"/>"></script>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>

<script src="<c:url value="/js/application.js"/>"></script>
