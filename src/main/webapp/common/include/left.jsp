<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" 	  uri="http://www.springframework.org/security/tags" %>

<nav id="nav" class="nav-primary visible-desktop nav-vertical bg-light">
	<ul class="nav">
	<!-- 	<li><a href="default.html"><i class="icon-dashboard icon-xlarge"></i>Dashboard</a></li> -->
	    <li class="dropdown-submenu">
	        <a href="<c:url value="/admin/getreports.do"/>"><i class="icon-th icon-xlarge"></i>리포트관리</a>
	<!--         <ul class="dropdown-menu"> -->
	<%-- 			<li><a href="<c:url value="/admin/getreports.do"/>">리포트 보기</a></li> --%>
	<!--           	<li><a href="icons.html"><b class="badge pull-right">리포트</b>설정</a></li>             -->
	<!--           	<li><a href="grid.html">사건관리</a></li> -->
	<!--         </ul> -->
		</li>
		
	     <li class="dropdown-submenu">
	     	<a href="<c:url value="/admin/getstatics.do"/>"><i class="icon-edit icon-xlarge"></i>통계추출</a>
     	     <ul class="dropdown-menu">
				<li><a href="<c:url value="/admin/getstatics.do"/>"> 리스트</a></li>
	          	<li><a href="<c:url value="/admin/stc/domain.do"/>">소속값관리</a></li>            
       		</ul>
	     </li>
	     
	     <sec:authorize ifAnyGranted="ROLE_MASTER">
	     	<li class="dropdown-submenu">
	     		<a href="#"><i class="icon-signal icon-xlarge"></i>고객사관리</a>
     			<ul class="dropdown-menu">
					<li><a href="<c:url value="/admin/cus/customs.do"/>">고객 리스트</a></li>
		          	<li><a href="#">결제현황</a></li>            
		          	<li><a href="#">포탈공지</a></li>
		          	<li><a href="#">포탈FAQ</a></li>
		          	<li><a href="#">포탈Q&A</a></li>
        		</ul>
	     	</li>
	     </sec:authorize>
	     
	     <li><a href="<c:url value="/admin/getboards.do"/>"><i class="icon-link icon-xlarge"></i>공지관리</a></li>
	     
	     
		     <li class="dropdown-submenu">
		     	<a href="<c:url value="/admin/getinfo.do"/>"><i class="icon-cog icon-xlarge"></i>계정관리</a>
	     		 <ul class="dropdown-menu">
					<li><a href="<c:url value="/admin/getinfo.do"/>">내정보</a></li>
					<sec:authorize ifAnyGranted="ROLE_MASTER">
			          	<li><a href="icons.html">사용자계정</a></li>     
			          	<li><a href="<c:url value="/admin/getmanagers.do"/>">Portal계정</a></li>   
		          	 </sec:authorize>   
		          	 <sec:authorize ifAnyGranted="ROLE_SUB_MASTER">
			          	<li><a href="icons.html">사용자계정</a></li>     
			          	<li><a href="<c:url value="/admin/acct/pageadd.do"/>">Portal계정</a></li>  		          	 
		          	 </sec:authorize>  
        		</ul>
		     </li>
	</ul>
</nav>