<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" 		  uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- footer -->
 <footer id="footer">
  <div class="text-center padder clearfix">
    <p>
      <small>&copy; Listen@Me 2018, Social Venture Technology for the Voice of Truth</small><br><br>
    </p>
  </div>
</footer>
<!-- / footer -->